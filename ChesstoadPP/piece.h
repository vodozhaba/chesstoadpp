#pragma once
#include "globals.h"

namespace chesstoadpp
{
	typedef std::uint8_t Piece;
	typedef std::uint8_t Color;

	class PieceTools
	{
	public:
		static constexpr Piece kRook = 0x0;
		static constexpr Piece kKnight = 0x2;
		static constexpr Piece kBishop = 0x4;
		static constexpr Piece kQueen = 0x6;
		static constexpr Piece kKing = 0x8;
		static constexpr Piece kPawn = 0xA;
		static constexpr Piece kNone = 0xE;
		static constexpr Color kWhite = 0x0;
		static constexpr Color kBlack = 0x1;
		/// <summary>
		/// Converts the specIfied character to a corresponding piece.
		/// </summary>
		/// <param name="c">The character.</param>
		/// <param name="ignore_color">If set to <c>true</c>, the returned piece is always white.</param>
		/// <returns>The piece.</returns>
		static Piece CharConstruct(char c, bool ignore_color = false);
		/// <summary>
		/// Converts the specIfied piece to a corresponding piece
		/// </summary>
		/// <param name="p">The p.</param>
		/// <param name="ignore_color">If set to <c>true</c>, the returned character is always lowercase.</param>
		/// <returns></returns>
		static char ToChar(Piece p, bool ignore_color = false);
		/// <summary>
		/// Returns the white piece of the same type as the specIfied piece.
		/// </summary>
		/// <param name="p">The original piece.</param>
		/// <returns>The white piece.</returns>
		static Piece Type(Piece p);
		/// <summary>
		/// Returns the color of the specIfied piece.
		/// </summary>
		/// <param name="p">The piece.</param>
		/// <returns>The color.</returns>
		static Color Color(Piece p);
	};

	inline Piece PieceTools::Type(const Piece p)
	{
		return p & 0xE;
	}

	inline Color PieceTools::Color(const Piece p)
	{
		return p & 0x1;
	}
}
