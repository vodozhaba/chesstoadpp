#include "globals.h"
#include "move.h"

namespace chesstoadpp
{
	Move MoveTools::StrConstruct(const std::string& s)
	{
		const auto len = s.length();
		if (len != 4 && len != 5)
			return kInvalid;
		const auto source = SquareTools::StrConstruct(s.substr(0, 2));
		const auto dest = SquareTools::StrConstruct(s.substr(2, 2));
		if (source == SquareTools::kInvalid || dest == SquareTools::kInvalid)
			return kInvalid;
		if (len == 4)
			return SquareConstruct(source, dest, PieceTools::kNone);
		if (len == 5)
		{
			const auto promotion = PieceTools::CharConstruct(s[4], true);
			if (promotion == PieceTools::kNone || promotion == PieceTools::kPawn || promotion == PieceTools::kKing)
				return kInvalid;
			return SquareConstruct(source, dest, promotion);
		}
		return kInvalid;
	}

	std::string MoveTools::ToString(const Move move)
	{
		if (move == kInvalid)
			return "0000";
		auto ret = SquareTools::ToString(Source(move)) + SquareTools::ToString(Dest(move));
		const auto promotion = PieceTools::ToChar(Promotion(move), true);
		if (promotion)
			ret.push_back(promotion);
		return ret;
	}
}
