#include "globals.h"
#include "piece.h"

namespace chesstoadpp
{
	char PieceTools::ToChar(const Piece p, const bool ignore_color)
	{
		switch (p | ignore_color) // -V564
		{
		case kRook | kWhite: return 'R';
		case kKnight | kWhite: return 'N';
		case kBishop | kWhite: return 'B';
		case kQueen | kWhite: return 'Q';
		case kKing | kWhite: return 'K';
		case kPawn | kWhite: return 'P';
		case kRook | kBlack: return 'r';
		case kKnight | kBlack: return 'n';
		case kBishop | kBlack: return 'b';
		case kQueen | kBlack: return 'q';
		case kKing | kBlack: return 'k';
		case kPawn | kBlack: return 'p';
		default: return '\0';
		}
	}

	Piece PieceTools::CharConstruct(const char c, const bool ignore_color)
	{
		switch (c)
		{
		case 'R': return kRook;
		case 'N': return kKnight;
		case 'B': return kBishop;
		case 'Q': return kQueen;
		case 'K': return kKing;
		case 'P': return kPawn;
		case 'r': return kRook | !ignore_color; //-V564
		case 'n': return kKnight | !ignore_color; //-V564
		case 'b': return kBishop | !ignore_color; //-V564
		case 'q': return kQueen | !ignore_color; //-V564
		case 'k': return kKing | !ignore_color; //-V564
		case 'p': return kPawn | !ignore_color; //-V564
		default: return kNone;
		}
	}
}
