#pragma once
#include "bitboard.h"

namespace chesstoadpp
{
	class MagicBitboards
	{
	public:

		/// <summary>
		/// Initializes relevant occupancy masks
		/// </summary>
		static void Init();

		/// <summary>
		/// Lists squares attacked by a rook at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard RookAttacks(Square square, Bitboard occupancy);

		/// <summary>
		/// Lists squares attacked by a bishop at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard BishopAttacks(Square square, Bitboard occupancy);

		/// <summary>
		/// Lists squares attacked by a queen at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard QueenAttacks(Square square, Bitboard occupancy);

	private:

		/// <summary>
		/// Magic numbers for hashing occupancy concerning rook attacks
		/// </summary>
		static const uint64_t kRookMagics[64];

		/// <summary>
		/// Magic numbers for hashing occupancy concerning bishop attacks
		/// </summary>
		static const uint64_t kBishopMagics[64];

		/// <summary>
		/// How far to shift to hash relevant occupancy for rook
		/// </summary>
		static const uint64_t kRookShifts[64];

		/// <summary>
		/// How far to shift to hash relevant occupancy for bishop
		/// </summary>
		static const uint64_t kBishopShifts[64];

		/// <summary>
		/// Relevant occupancy masks for rook (N, E, S, W)
		/// </summary>
		static Bitboard rook_relevant_occupancy_masks_[64];

		/// <summary>
		/// Relevant occupancy masks for bishop (NW, NE, SW, SE)
		/// </summary>
		static Bitboard bishop_relevant_occupancy_masks_[64];
		
		/// <summary>
		/// Fancy magic bitboards for rook on a per-square basis
		/// </summary>
		static std::vector<Bitboard> rook_attack_table_[64];
		
		/// <summary>
		/// Fancy magic bitboards for bishop on a per-square basis
		/// </summary>
		static std::vector<Bitboard> bishop_attack_table_[64];

		/// <summary>
		/// Calculates a bitboard where every bit from the sparse mask is set to a corresponding bit from the dense value.
		/// E.g. SpreadBits(0b00001111, 0b0101010101010101) = 0b0000000001010101
		/// </summary>
		/// <param name="dense">Bits in the dense form.</param>
		/// <param name="sparse_mask">Bits to set in the output.</param>
		/// <returns></returns>
		static Bitboard SpreadBits(Bitboard dense, Bitboard sparse_mask);

	};


	inline Bitboard MagicBitboards::RookAttacks(const Square square, const Bitboard occupancy)
	{
		const auto relevant_occupancy = occupancy & rook_relevant_occupancy_masks_[square];
		const auto hash = (relevant_occupancy * kRookMagics[square]) >> kRookShifts[square];
		return rook_attack_table_[square][hash];
	}

	inline Bitboard MagicBitboards::BishopAttacks(const Square square, const Bitboard occupancy)
	{
		const auto relevant_occupancy = occupancy & bishop_relevant_occupancy_masks_[square];
		const auto hash = (relevant_occupancy * kBishopMagics[square]) >> kBishopShifts[square];
		return bishop_attack_table_[square][hash];
	}

	inline Bitboard MagicBitboards::QueenAttacks(const Square square, const Bitboard occupancy)
	{
		return RookAttacks(square, occupancy) | BishopAttacks(square, occupancy);
	}

}
