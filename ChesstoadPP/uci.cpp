#include "globals.h"
#include "uci.h"
#include "precalc.h"
#include "transposition_table.h"
#include "magic_bitboards.h"

namespace chesstoadpp
{
	Uci Uci::uci_singleton_(std::cin, std::cout);

	Uci::Uci(std::istream& is, std::ostream& os) : is_(is), os_(os)
	{
		is_running_ = true;
		initialize_on_next_cmd_ = false;
		debug_ = false;
		is_analyzing_ = false;
		discard_best_move_ = false;
	}

	void Uci::Run()
	{
		std::string cmd, option;
		while (is_running_)
		{
			std::getline(is_, cmd, '\n');
			std::istringstream cmd_stream(cmd);
			cmd_stream >> cmd;
			if (cmd == "quit") {
				CmdQuit();
			}
			else if(initialize_on_next_cmd_)
			{
				PrecalcBitboards::Init();
				MagicBitboards::Init();
				TranspositionTable::Init();
				initialize_on_next_cmd_ = false;
			}
			std::queue<std::string> options;
			while (std::getline(cmd_stream, option, ' '))
			{
				if (option.empty())
					continue;
				options.push(option);
			}
			if (cmd == "uci")
				CmdUci();
			else if (cmd == "debug")
				CmdDebug(options);
			else if (cmd == "isready")
				CmdIsReady();
			else if (cmd == "setoption")
				CmdSetOption(options);
			else if (cmd == "register")
				CmdRegister(options);
			else if (cmd == "ucinewgame")
				CmdUciNewGame();
			else if (cmd == "position")
				CmdPosition(options);
			else if (cmd == "go")
				CmdGo(options);
			else if (cmd == "stop")
				CmdStop();
			else if (cmd == "ponderhit")
				CmdPonderHit();
		}
	}

	void Uci::CmdUci()
	{
		os_ << "id name ChesstoadPP development snapshot" << std::endl;
		os_ << "id author Vasily Svinko" << std::endl;
		os_ << "option name Hash type spin default 1 min 1 max 12288" << std::endl;
		os_ << "uciok" << std::endl;
		initialize_on_next_cmd_ = true;
	}

	void Uci::CmdDebug(std::queue<std::string>& options)
	{
		if (options.front() == "on")
			debug_ = true;
		else if (options.front() == "off")
			debug_ = false;
	}

	void Uci::CmdIsReady() const
	{
		os_ << "readyok" << std::endl;
	}

	void Uci::CmdSetOption(std::queue<std::string>& options)
	{
		options.pop();
		const auto name = options.front();
		options.pop();
		if (name == "Hash")
		{
			options.pop();
			TranspositionTable::transposition_table_singleton_->Resize(std::stoull(options.front()) * 1024 * 1024);
		}
	}


	void Uci::CmdRegister(std::queue<std::string>& options) const
	{
		os_ << "registration checking" << std::endl;
		os_ << "registration ok" << std::endl;
	}

	void Uci::CmdUciNewGame()
	{
	}

	void Uci::CmdPosition(std::queue<std::string>& options)
	{
		if (analysis_)
		{
			discard_best_move_ = true;
			StopAnalysis();
		}
		if (options.front() == "fen")
		{
			options.pop();
			std::stringstream ss;
			ss << options.front();
			options.pop();
			for (auto i = 0; i < 5; ++i)
			{
				ss << ' ';
				ss << options.front();
				options.pop();
			}
			pos_ = std::make_unique<Position>(ss.str());
		}
		else if (options.front() == "startpos")
		{
			options.pop();
			pos_ = std::make_unique<Position>();
		}
		if (options.empty())
			return;
		if (options.front() == "moves")
		{
			options.pop();
			while (!options.empty())
			{
				auto move = MoveTools::StrConstruct(options.front());
				if (move == MoveTools::kInvalid)
					break;
				auto& legal_moves = pos_->ListMoves();
				if (std::find(legal_moves.begin(), legal_moves.end(), move) == legal_moves.end())
					break;
				pos_->Make(move);
				options.pop();
			}
		}
	}

	void Uci::CmdGo(std::queue<std::string>& options)
	{
		const auto legal_moves = pos_->ListMoves();
		auto moves = legal_moves;
		auto depth = 5;
		while (!options.empty())
		{
			if (options.front() == "searchmoves")
			{
				options.pop();
				moves.clear();
				while (!options.empty())
				{
					const auto move = MoveTools::StrConstruct(options.front());
					if (move == MoveTools::kInvalid)
						break;
					options.pop();
					if (std::find(legal_moves.begin(), legal_moves.end(), move) == legal_moves.end())
						continue;
					moves.push_back(move);
				}
			}
			else if (options.front() == "ponder")
			{
			}
			else if (options.front() == "wtime")
			{
				options.pop();
				options.pop();
			}
			else if (options.front() == "btime")
			{
				options.pop();
				options.pop();
			}
			else if (options.front() == "winc")
			{
				options.pop();
				options.pop();
			}
			else if (options.front() == "binc")
			{
				options.pop();
				options.pop();
			}
			else if (options.front() == "depth")
			{
				options.pop();
				depth = std::stoi(options.front());
				options.pop();
			}
			else if (options.front() == "nodes")
			{
			}
			else if (options.front() == "mate")
			{
			}
			else if (options.front() == "infinite")
			{
				depth = -1;
				options.pop();
			}
		}
		StartAnalysis(moves, depth);
	}

	void Uci::CmdStop()
	{
		StopAnalysis();
	}

	void Uci::CmdPonderHit()
	{
	}

	void Uci::CmdQuit()
	{
		is_running_ = false;
		if (is_analyzing_)
			StopAnalysis();
	}

	void Uci::StartAnalysis(const std::deque<Move>& moves, int depth)
	{
		if (analysis_)
		{
			discard_best_move_ = true;
			StopAnalysis();
		}
		is_analyzing_ = true;
		discard_best_move_ = false; //-V519
		analysis_ = std::make_unique<std::future<Move>>(
			std::async(std::launch::async, &Position::BestMoveIterative, pos_.get(), moves, depth));
	}

	void Uci::StopAnalysis()
	{
		is_analyzing_ = false;
		analysis_->wait();
	}

	void Uci::BestMove(const Move move) const
	{
		if (discard_best_move_)
			return;
		os_ << "bestmove " << MoveTools::ToString(move) << std::endl;
	}
}
