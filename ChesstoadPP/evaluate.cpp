#include "globals.h"
#include "position.h"
#include "precalc.h"

namespace chesstoadpp
{
	static const int kKnightPositionBonus[] = {
		-50, -40, -30, -30, -30, -30, -40, -50,
		-40, -20, 0, 0, 0, 0, -20, -40,
		-30, 0, 10, 15, 15, 10, 0, -30,
		-30, 5, 15, 20, 20, 15, 5, -30,
		-30, 0, 15, 20, 20, 15, 0, -30,
		-30, 5, 10, 15, 15, 10, 5, -30,
		-40, -20, 0, 5, 5, 0, -20, -40,
		-50, -40, -30, -30, -30, -30, -40, -50,
	};

	static const int kPawnPositionBonus[] = {
		0, 0, 0, 0, 0, 0, 0, 0,
		50, 50, 50, 50, 50, 50, 50, 50,
		10, 10, 20, 30, 30, 20, 10, 10,
		5, 5, 10, 25, 25, 10, 5, 5,
		0, 0, 0, 20, 20, 0, 0, 0,
		5, -5, -10, 0, 0, -10, -5, 5,
		5, 10, 10, -20, -20, 10, 10, 5,
		0, 0, 0, 0, 0, 0, 0, 0
	};

	static const int kRookPositionBonus[] = {
		0, 0, 0, 0, 0, 0, 0, 0,
		5, 10, 10, 10, 10, 10, 10, 5,
		-5, 0, 0, 0, 0, 0, 0, -5,
		-5, 0, 0, 0, 0, 0, 0, -5,
		-5, 0, 0, 0, 0, 0, 0, -5,
		-5, 0, 0, 0, 0, 0, 0, -5,
		-5, 0, 0, 0, 0, 0, 0, -5,
		0, 0, 0, 5, 5, 0, 0, 0
	};

	static const int kQueenPositionBonus[] = {
		-20, -10, -10, -5, -5, -10, -10, -20,
		-10, 0, 0, 0, 0, 0, 0, -10,
		-10, 0, 5, 5, 5, 5, 0, -10,
		-5, 0, 5, 5, 5, 5, 0, -5,
		0, 0, 5, 5, 5, 5, 0, -5,
		-10, 5, 5, 5, 5, 5, 0, -10,
		-10, 0, 5, 0, 0, 0, 0, -10,
		-20, -10, -10, -5, -5, -10, -10, -20
	};

	static const int kKingMiddleGamePositionBonus[] = {
		-30, -40, -40, -50, -50, -40, -40, -30,
		-30, -40, -40, -50, -50, -40, -40, -30,
		-30, -40, -40, -50, -50, -40, -40, -30,
		-30, -40, -40, -50, -50, -40, -40, -30,
		-20, -30, -30, -40, -40, -30, -30, -20,
		-10, -20, -20, -20, -20, -20, -20, -10,
		20, 20, 0, 0, 0, 0, 20, 20,
		20, 30, 10, 0, 0, 10, 30, 20
	};

	static const int kKingEndgamePositionBonus[]{
		-50, -40, -30, -20, -20, -30, -40, -50,
		-30, -20, -10, 0, 0, -10, -20, -30,
		-30, -10, 20, 30, 30, 20, -10, -30,
		-30, -10, 30, 40, 40, 30, -10, -30,
		-30, -10, 30, 40, 40, 30, -10, -30,
		-30, -10, 20, 30, 30, 20, -10, -30,
		-30, -30, 0, 0, 0, 0, -30, -30,
		-50, -30, -30, -30, -30, -30, -30, -50
	};

	int Position::Evaluate()
	{
		Analyze();
		if (checkmate_)
			return -(INT_MAX / 2);
		if (IsDraw())
			return 0;
		auto score = 0;
		score += 100 * BitboardTools::Count(bitboards_[PieceTools::kPawn | current_player_]);
		score -= 100 * BitboardTools::Count(bitboards_[PieceTools::kPawn | opponent_]);
		score += 320 * BitboardTools::Count(bitboards_[PieceTools::kKnight | current_player_]);
		score -= 320 * BitboardTools::Count(bitboards_[PieceTools::kKnight | opponent_]);
		score += 330 * BitboardTools::Count(bitboards_[PieceTools::kBishop | current_player_]);
		score -= 330 * BitboardTools::Count(bitboards_[PieceTools::kBishop | opponent_]);
		score += 500 * BitboardTools::Count(bitboards_[PieceTools::kRook | current_player_]);
		score -= 500 * BitboardTools::Count(bitboards_[PieceTools::kRook | opponent_]);
		score += 900 * BitboardTools::Count(bitboards_[PieceTools::kQueen | current_player_]);
		score -= 900 * BitboardTools::Count(bitboards_[PieceTools::kQueen | opponent_]);

		const auto flip_tables = current_player_ == PieceTools::kBlack;

		auto pawns = bitboards_[PieceTools::kPawn | current_player_];
		auto offset = 0;
		while (pawns)
		{
			auto square = BitboardTools::BitScanForward(pawns);
			if (square != 63)
			{
				pawns >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				pawns = BitboardTools::kEmpty;
			}
			const auto bonus = kPawnPositionBonus[flip_tables ? square : 63 - square];
			score += bonus;
		}
		pawns = bitboards_[PieceTools::kPawn | opponent_];
		offset = 0;
		while (pawns)
		{
			auto square = BitboardTools::BitScanForward(pawns);
			if (square != 63)
			{
				pawns >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				pawns = BitboardTools::kEmpty;
			}
			const auto bonus = kPawnPositionBonus[flip_tables ? 63 - square : square];
			score -= bonus;
		}

		auto knights = bitboards_[PieceTools::kKnight | current_player_];
		offset = 0;
		while (knights)
		{
			auto square = BitboardTools::BitScanForward(knights);
			if (square != 63)
			{
				knights >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				knights = BitboardTools::kEmpty;
			}
			const auto bonus = kKnightPositionBonus[flip_tables ? square : 63 - square];
			score += bonus;
		}
		knights = bitboards_[PieceTools::kKnight | opponent_];
		offset = 0;
		while (knights)
		{
			auto square = BitboardTools::BitScanForward(knights);
			if (square != 63)
			{
				knights >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				knights = BitboardTools::kEmpty;
			}
			const auto bonus = kKnightPositionBonus[flip_tables ? 63 - square : square];
			score -= bonus;
		}

		auto rooks = bitboards_[PieceTools::kRook | current_player_];
		offset = 0;
		while (rooks)
		{
			auto square = BitboardTools::BitScanForward(rooks);
			if (square != 63)
			{
				rooks >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				rooks = BitboardTools::kEmpty;
			}
			const auto bonus = kRookPositionBonus[flip_tables ? square : 63 - square];
			score += bonus;
		}
		rooks = bitboards_[PieceTools::kRook | opponent_];
		offset = 0;
		while (rooks)
		{
			auto square = BitboardTools::BitScanForward(rooks);
			if (square != 63)
			{
				rooks >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				rooks = BitboardTools::kEmpty;
			}
			const auto bonus = kRookPositionBonus[flip_tables ? 63 - square : square];
			score -= bonus;
		}

		auto& king_position_bonus = endgame_ ? kKingEndgamePositionBonus : kKingMiddleGamePositionBonus;
		auto king = bitboards_[PieceTools::kKing | current_player_];
		auto square = BitboardTools::BitScanForward(king);
		auto bonus = king_position_bonus[flip_tables ? square : 63 - square];
		score += bonus;
		king = bitboards_[PieceTools::kKing | opponent_];
		square = BitboardTools::BitScanForward(king);
		bonus = king_position_bonus[flip_tables ? 63 - square : square];
		score -= bonus;
		return score;
	}
}
