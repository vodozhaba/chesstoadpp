#pragma once

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <cstdlib>
#include <deque>
#include <fstream>
#include <future>
#include <iostream>
#include <iterator>
#include <queue>
#include <sstream>
#include <thread>
#include <utility>
#include <intrin.h>
