#include "globals.h"
#include "square.h"

namespace chesstoadpp
{
	Square SquareTools::StrConstruct(const std::string& s)
	{
		if (s.length() != 2 || s[0] < 'a' || s[0] > 'h' || s[1] < '1' || s[1] > '8')
			return kInvalid;
		return CoordConstruct(s[1] - '1', s[0] - 'a');
	}

	std::string SquareTools::ToString(const Square s)
	{
		std::string ret;
		ret.push_back("abcdefgh"[File(s)]);
		ret.push_back("12345678"[Rank(s)]);
		return ret;
	}
}
