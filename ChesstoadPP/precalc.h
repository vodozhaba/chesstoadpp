#pragma once
#include "bitboard.h"
#include "position.h"

namespace chesstoadpp
{
	class PrecalcBitboards
	{
	public:
		/// <summary>
		/// Calculates rays and non-sliding piece attacks
		/// </summary>
		static void Init();
		/// <summary>
		/// Lists squares attacked by a rook at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard RookAttacks(Square square, Bitboard occupancy);
		/// <summary>
		/// Lists squares attacked by a bishop at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard BishopAttacks(Square square, Bitboard occupancy);
		/// <summary>
		/// Lists squares attacked by a queen at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="occupancy">All occupied squares on the board.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard QueenAttacks(Square square, Bitboard occupancy);
		/// <summary>
		/// Lists squares attacked by a knight at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard KnightAttacks(Square square);
		/// <summary>
		/// Lists squares attacked by a king at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard KingAttacks(Square square); // The king attacks!
		/// <summary>
		/// Lists squares attacked by a pawn of the specified color at the specified square.
		/// </summary>
		/// <param name="square">The source square.</param>
		/// <param name="color">The pawn's color.</param>
		/// <returns>Attacked squares.</returns>
		static Bitboard PawnAttacks(Square square, Color color);
	private:
		static Bitboard rays_[8][64];
		static constexpr int kNoWe = 0;
		static constexpr int kNo = 1;
		static constexpr int kNoEa = 2;
		static constexpr int kEa = 3;
		static constexpr int kSoEa = 4;
		static constexpr int kSo = 5;
		static constexpr int kSoWe = 6;
		static constexpr int kWe = 7;
		static Bitboard knight_attacks_[64];
		static Bitboard king_attacks_[64];
		static Bitboard pawn_attacks_[64];
	};

	inline Bitboard PrecalcBitboards::KnightAttacks(const Square square)
	{
		return knight_attacks_[square];
	}

	inline Bitboard PrecalcBitboards::KingAttacks(const Square square)
	{
		return king_attacks_[square];
	}

	inline Bitboard PrecalcBitboards::PawnAttacks(Square square, const Color color)
	{
		const auto y_dir = 1 - (color << 1);
		square = SquareTools::Shift(square, y_dir, 0);
		return square == 0xFF ? BitboardTools::kEmpty : pawn_attacks_[square];
	}
}
