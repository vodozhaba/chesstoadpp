#pragma once
#include "globals.h"
#include "square.h"
#pragma intrinsic(_BitScanForward64)
#pragma intrinsic(_BitScanReverse64)

namespace chesstoadpp
{
	typedef std::uint64_t Bitboard;

	class BitboardTools
	{
	public:
		/// <summary>
		/// An empty bitboard.
		/// </summary>
		static constexpr Bitboard kEmpty = 0x0000000000000000ULL;
		/// <summary>
		/// A fully-occupied bitboard.
		/// </summary>
		static constexpr Bitboard kUniverse = 0xFFFFFFFFFFFFFFFFULL;
		/// <summary>
		/// Calculates a bitboard with only the specified square occupied.
		/// </summary>
		/// <param name="square">The square.</param>
		/// <returns>The bitboard.</returns>
		static Bitboard SingleSquare(int square);
		/// <summary>
		/// Finds the first occupied square in the specified bitboard.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <returns>The square. Undefined if not found.</returns>
		static Square BitScanForward(Bitboard bb);
		/// <summary>
		/// Finds the last occupied square in the specified bitboard.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <returns>The square. Undefined if not found.</returns>
		static Square BitScanReverse(Bitboard bb);
		/// <summary>
		/// Counts the number of occupied squares in the specified bitboard.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <returns>Count of occupied squares..</returns>
		static unsigned int Count(Bitboard bb);
		/// <summary>
		/// Shifts the specified bitboard specified number of ranks up.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="ranks">Number of ranks.</param>
		/// <returns>The shifted bitboard.</returns>
		static Bitboard ShiftUp(Bitboard bb, unsigned int ranks = 1);
		/// <summary>
		/// Shifts the specified bitboard specified number of ranks down.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="ranks">Number of ranks.</param>
		/// <returns>The shifted bitboard.</returns>
		static Bitboard ShiftDown(Bitboard bb, unsigned int ranks = 1);
		/// <summary>
		/// Shifts the specified bitboard specified number of files left.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="files">Number of files.</param>
		/// <returns>The shifted bitboard.</returns>
		static Bitboard ShiftLeft(Bitboard bb, unsigned int files = 1);
		/// <summary>
		/// Shifts the specified bitboard specified number of files right.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="files">Number of files.</param>
		/// <returns>The shifted bitboard.</returns>
		static Bitboard ShiftRight(Bitboard bb, unsigned int files = 1);
		/// <summary>
		/// Shifts the specified bitboard specified number of ranks, up if positive and down if negative.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="ranks">Number of ranks.</param>
		/// <returns></returns>
		static Bitboard ShiftV(Bitboard bb, int ranks);
		/// <summary>
		/// Shifts the specified bitboard specified number of files, right if positive and left if negative.
		/// </summary>
		/// <param name="bb">The bitboard.</param>
		/// <param name="files">Number of files.</param>
		/// <returns></returns>
		static Bitboard ShiftH(Bitboard bb, int files);
	};

	inline Bitboard BitboardTools::SingleSquare(const int square)
	{
		return 1ULL << square;
	}

	inline Square BitboardTools::BitScanForward(const Bitboard bb)
	{
		unsigned long ret;
		_BitScanForward64(&ret, bb);
		return ret;
	}

	inline Square BitboardTools::BitScanReverse(const Bitboard bb)
	{
		unsigned long ret;
		_BitScanReverse64(&ret, bb);
		return ret;
	}

	inline unsigned int BitboardTools::Count(const Bitboard bb)
	{
		return __popcnt64(bb);
	}

	inline Bitboard BitboardTools::ShiftUp(const Bitboard bb, const unsigned int ranks)
	{
		if (ranks > 7)
			return kEmpty;
		return bb << (ranks * 8);
	}

	inline Bitboard BitboardTools::ShiftDown(const Bitboard bb, const unsigned int ranks)
	{
		if (ranks > 7)
			return kEmpty;
		return bb >> (ranks * 8);
	}

	inline Bitboard BitboardTools::ShiftLeft(Bitboard bb, const unsigned int files)
	{
		if (files > 7)
			return kEmpty;
		const auto mask = ((1 << files) - 1) * 0x0101010101010101ULL;
		return (bb & ~mask) >> files;
	}

	inline Bitboard BitboardTools::ShiftRight(const Bitboard bb, const unsigned int files)
	{
		if (files > 7)
			return kEmpty;
		const auto mask = ((1 << files) - 1) * 0x0101010101010101ULL;
		return (bb << files) & ~mask;
	}
}
