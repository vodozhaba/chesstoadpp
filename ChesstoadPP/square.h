#pragma once
#include "globals.h"
#include <string>

namespace chesstoadpp
{
	typedef std::uint8_t Square;

	class SquareTools
	{
	public:
		static constexpr Square kA1 = 0x00;
		static constexpr Square kB1 = 0x01;
		static constexpr Square kC1 = 0x02;
		static constexpr Square kD1 = 0x03;
		static constexpr Square kE1 = 0x04;
		static constexpr Square kF1 = 0x05;
		static constexpr Square kG1 = 0x06;
		static constexpr Square kH1 = 0x07;
		static constexpr Square kA2 = 0x08;
		static constexpr Square kB2 = 0x09;
		static constexpr Square kC2 = 0x0A;
		static constexpr Square kD2 = 0x0B;
		static constexpr Square kE2 = 0x0C;
		static constexpr Square kF2 = 0x0D;
		static constexpr Square kG2 = 0x0E;
		static constexpr Square kH2 = 0x0F;
		static constexpr Square kA3 = 0x10;
		static constexpr Square kB3 = 0x11;
		static constexpr Square kC3 = 0x12;
		static constexpr Square kD3 = 0x13;
		static constexpr Square kE3 = 0x14;
		static constexpr Square kF3 = 0x15;
		static constexpr Square kG3 = 0x16;
		static constexpr Square kH3 = 0x17;
		static constexpr Square kA4 = 0x18;
		static constexpr Square kB4 = 0x19;
		static constexpr Square kC4 = 0x1A;
		static constexpr Square kD4 = 0x1B;
		static constexpr Square kE4 = 0x1C;
		static constexpr Square kF4 = 0x1D;
		static constexpr Square kG4 = 0x1E;
		static constexpr Square kH4 = 0x1F;
		static constexpr Square kA5 = 0x20;
		static constexpr Square kB5 = 0x21;
		static constexpr Square kC5 = 0x22;
		static constexpr Square kD5 = 0x23;
		static constexpr Square kE5 = 0x24;
		static constexpr Square kF5 = 0x25;
		static constexpr Square kG5 = 0x26;
		static constexpr Square kH5 = 0x27;
		static constexpr Square kA6 = 0x28;
		static constexpr Square kB6 = 0x29;
		static constexpr Square kC6 = 0x2A;
		static constexpr Square kD6 = 0x2B;
		static constexpr Square kE6 = 0x2C;
		static constexpr Square kF6 = 0x2D;
		static constexpr Square kG6 = 0x2E;
		static constexpr Square kH6 = 0x2F;
		static constexpr Square kA7 = 0x30;
		static constexpr Square kB7 = 0x31;
		static constexpr Square kC7 = 0x32;
		static constexpr Square kD7 = 0x33;
		static constexpr Square kE7 = 0x34;
		static constexpr Square kF7 = 0x35;
		static constexpr Square kG7 = 0x36;
		static constexpr Square kH7 = 0x37;
		static constexpr Square kA8 = 0x38;
		static constexpr Square kB8 = 0x39;
		static constexpr Square kC8 = 0x3A;
		static constexpr Square kD8 = 0x3B;
		static constexpr Square kE8 = 0x3C;
		static constexpr Square kF8 = 0x3D;
		static constexpr Square kG8 = 0x3E;
		static constexpr Square kH8 = 0x3F;
		/// <summary>
		/// Invalid square.
		/// </summary>
		static constexpr Square kInvalid = 0xFF;
		/// <summary>
		/// Constructs a square from a string (e.g. "e2")
		/// </summary>
		/// <param name="s">The string.</param>
		/// <returns>The square.</returns>
		static Square StrConstruct(const std::string& s);
		/// <summary>
		/// Constructs a square from its coordinates.
		/// </summary>
		/// <param name="rank">The rank.</param>
		/// <param name="file">The file.</param>
		/// <returns>The square.</returns>
		static Square CoordConstruct(int rank, int file);
		/// <summary>
		/// Converts the specified square to a string.
		/// </summary>
		/// <param name="s">The square.</param>
		/// <returns>The string</returns>
		static std::string ToString(Square s);
		/// <summary>
		/// Extracts the specified square's rank.
		/// </summary>
		/// <param name="s">The square.</param>
		/// <returns>The square's rank.</returns>
		static int Rank(Square s);
		/// <summary>
		/// Extracts the specified square's file.
		/// </summary>
		/// <param name="s">The square.</param>
		/// <returns>The square's file.</returns>
		static int File(Square s);
		/// <summary>
		/// Moves the specified squares in the specified direction.
		/// </summary>
		/// <param name="s">The original square.</param>
		/// <param name="ranks">Number of ranks to shift (up if positive, down if negative).</param>
		/// <param name="files">Number of files to shift (right if positive, left if negative)</param>
		/// <returns>The shifted square.</returns>
		static Square Shift(Square s, int ranks, int files);
	};

	inline Square SquareTools::CoordConstruct(const int rank, const int file)
	{
		return (rank << 3) + file;
	}

	inline int SquareTools::Rank(Square s)
	{
		return s >> 3;
	}

	inline int SquareTools::File(Square s)
	{
		return s & 0x07;
	}

	inline Square SquareTools::Shift(Square s, const int ranks, const int files)
	{
		s = s + 8 * ranks + files;
		return s & 0xC0 ? kInvalid : s;
	}
}
