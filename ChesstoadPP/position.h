#pragma once
#include "globals.h"
#include "bitboard.h"
#include "piece.h"
#include "move.h"

namespace chesstoadpp
{
	typedef uint8_t CastlingPermissions;

	class CastlingPermissionsTools
	{
	public:
		static constexpr CastlingPermissions kColorMask = 0x01;
		static constexpr CastlingPermissions kDirectionMask = 0x02;
		static constexpr CastlingPermissions kShortCastling = 0x00;
		static constexpr CastlingPermissions kLongCastling = 0x02;
	};

	class Position
	{
		friend class TranspositionTable;
	public:
		/// <summary>
		/// Initializes a new instance of the <see cref="Position"/> class based on a FEN string.
		/// </summary>
		/// <param name="fen">The FEN string.</param>
		Position(const std::string& fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
		/// <summary>
		/// Initializes a new instance of the <see cref="Position"/> class based on another instance.
		/// </summary>
		/// <param name="other">The source position.</param>
		/// <param name="preserve_analysis">if set to <c>true</c>, analysis data is copied to the new positoin.</param>
		Position(const Position& other, bool preserve_analysis = false);
		/// <summary>
		/// Makes the specified move.
		/// </summary>
		/// <param name="move">The move.</param>
		/// <returns>This instance.</returns>
		void Make(Move move);
		/// <summary>
		/// Lists the legal moves from current position.
		/// </summary>
		/// <returns>Double-ended queue of legal moves.</returns>
		const std::deque<Move>& ListMoves();
		/// <summary>
		/// Copies a position.
		/// </summary>
		/// <param name="other">The source position.</param>
		/// <returns>This instance.</returns>
		Position& operator=(const Position& other);
		/// <summary>
		/// Determines whether the current player's king is in check.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current player's king is in check; otherwise, <c>false</c>.
		/// </returns> 
		bool IsCheck();
		/// <summary>
		/// Determines whether the current player is checkmated.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current player is checkmated; otherwise, <c>false</c>.
		/// </returns>
		bool IsCheckMate();
		/// <summary>
		/// Determines whether the game has ended in a draw.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the game has ended in a draw; otherwise, <c>false</c>.
		/// </returns>
		bool IsDraw();
		/// <summary>
		/// Determines whether the game has ended.
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is game over]; otherwise, <c>false</c>.
		/// </returns>
		bool IsGameOver();
		/// <summary>
		/// Statically evaluates this instance.
		/// </summary>
		/// <returns>Score in centipawns.</returns>
		int Evaluate();
		/// <summary>
		/// Gets this instance's Zobrist hash.
		/// </summary>
		/// <returns>Zobrist hash.</returns>
		uint64_t GetHash() const;
		/// <summary>
		/// Iteratively looks for the best move from the specified up to a certain depth.
		/// </summary>
		/// <param name="moves">Moves to analyze.</param>
		/// <param name="depth">Max depth.</param>
		/// <returns>The best move.</returns>
		Move BestMoveIterative(const std::deque<Move>& moves, int depth);
	private:
		static const Move kWhiteShortCastling;
		static const Move kWhiteLongCastling;
		static const Move kBlackShortCastling;
		static const Move kBlackLongCastling;
		/// <summary>
		/// Occupancy for each type of pieces.
		/// </summary>
		Bitboard bitboards_[12];
		/// <summary>
		/// Color of the current player's pieces.
		/// </summary>
		Color current_player_;
		/// <summary>
		/// Color of the opponent's pieces.
		/// </summary>
		Color opponent_;
		/// <summary>
		/// Bitmap of castling rights:
		/// 0x01 - white short castling
		/// 0x02 - black short castling
		/// 0x04 - white long castling
		/// 0x08 - black long castling
		/// </summary>
		CastlingPermissions castling_;
		/// <summary>
		/// If the last move was a double pawn step, the passed square. Invalid square otherwise.
		/// </summary>
		Square en_passant_;
		/// <summary>
		/// Bitboard containing the <see cref="en_passant_"/> square.
		/// </summary>
		Bitboard en_passant_bb_;
		/// <summary>
		/// Current move number.
		/// </summary>
		int current_move_;
		/// <summary>
		/// Moves since the last pawn move or capture. Used for evaluating the 50 moves rule.
		/// </summary>
		int last_reversible_moves_;
		/// <summary>
		/// Current player's pieces.
		/// </summary>
		Bitboard current_player_occupancy_;
		/// <summary>
		/// Opponent's pieces.
		/// </summary>
		Bitboard opponent_occupancy_;
		/// <summary>
		/// All pieces on the board.
		/// </summary>
		Bitboard occupancy_;
		/// <summary>
		/// White and black pieces separately.
		/// </summary>
		Bitboard color_based_occupancy_[2];
		/// <summary>
		/// Squares attacked by the opponent.
		/// </summary>
		Bitboard attacked_;
		/// <summary>
		/// Squares attacked by the current player.
		/// </summary>
		Bitboard attacking_;
		/// <summary>
		/// Legal moves from the current position.
		/// </summary>
		std::deque<Move> possible_moves_;
		/// <summary>
		/// Current player's king is in check.
		/// </summary>
		bool check_;
		/// <summary>
		/// Current player is checkmated.
		/// </summary>
		bool checkmate_;
		/// <summary>
		/// Current player has no moves and not in check.
		/// </summary>
		bool stalemate_;
		/// <summary>
		/// There is at most one minor piece on the board and no pawns.
		/// </summary>
		bool insuf_material_;
		/// <summary>
		/// Current position has been repeated thrice during the game.
		/// </summary>
		bool threefold_repetition_;
		/// <summary>
		/// Current game is in the opening/middle game stage.
		/// </summary>
		bool middle_game_;
		/// <summary>
		/// Current game is in the endgame stage.
		/// </summary>
		bool endgame_;
		/// <summary>
		/// Current position has been analyzed.
		/// </summary>
		bool analyzed_;
		/// <summary>
		/// Current position's Zobrist hash.
		/// </summary>
		uint64_t hash_;
		/// <summary>
		/// All Zobrist hashes that this position has had throughout the game. Used for detecting threefold repetition.
		/// </summary>
		std::vector<uint64_t> hash_history_;
		/// <summary>
		/// Initializes an instance of the <see cref="Position"/> class based on another instance.
		/// </summary>
		/// <param name="other">The source position.</param>
		/// <param name="preserve_analysis">if set to <c>true</c>, analysis data is copied to the new position.</param>
		void Init(const Position& other, bool preserve_analysis);
		/// <summary>
		/// Populate metadata fields (e.g. <see cref="check_"/>)
		/// </summary>
		/// <param name="generate_moves">if set to <c>true</c> search for legal moves.</param>
		void Analyze(bool generate_moves = true);
		/// <summary>
		/// Handles pawn move side-effects.
		/// </summary>
		/// <param name="source">The source square.</param>
		/// <param name="dest">The destination square.</param>
		/// <param name="promotion">The piece to promote to.</param>
		/// <param name="piece">Reference to the piece variable. Used for promotion.</param>
		/// <returns>Set to <c>true</c> if the current move was NOT a double step; <c>false</c> otherwise.</returns>
		bool HandlePawnMove(Square source, Square dest, Piece promotion, Piece& piece);
		/// <summary>
		/// Handles king move side-effects.
		/// </summary>
		/// <param name="color">The king's color.</param>
		/// <param name="move">The move.</param>
		void HandleKingMove(Color color, Move move);
		/// <summary>
		/// Handles rook move side-effects.
		/// </summary>
		/// <param name="source">The source square.</param>
		void HandleRookMove(Square source);
		/// <summary>
		/// Populates <see cref="possible_moves_"/> with legal moves from the current position.
		/// </summary>
		void GenerateMoves();
		/// <summary>
		/// Adds pseudo-legal rook moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GenerateRookMoves();
		/// <summary>
		/// Adds pseudo-legal knight moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GenerateKnightMoves();
		/// <summary>
		/// Adds pseudo-legal bishop moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GenerateBishopMoves();
		/// <summary>
		/// Adds pseudo-legal queen moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GenerateQueenMoves();
		/// <summary>
		/// Adds pseudo-legal king moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GenerateKingMoves();
		/// <summary>
		/// Adds pseudo-legal pawn moves to <see cref="possible_moves_"/>.
		/// </summary>
		void GeneratePawnMoves();
		/// <summary>
		/// Adds all moves from the specified square to the squares in the destination bitboards.
		/// </summary>
		/// <param name="source">The source square.</param>
		/// <param name="dests">The destination squares.</param>
		/// <param name="promotion">The piece to promote to.</param>
		void AddMoves(Square source, Bitboard dests, Piece promotion = PieceTools::kNone);
		/// <summary>
		/// Populates <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateAttacks();
		/// <summary>
		/// Adds rook attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateRookAttacks();
		/// <summary>
		/// Adds knight attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateKnightAttacks();
		/// <summary>
		/// Adds bishop attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateBishopAttacks();
		/// <summary>
		/// Adds queen attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateQueenAttacks();
		/// <summary>
		/// Adds king attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GenerateKingAttacks();
		/// <summary>
		/// Adds pawn attacks to <see cref="attacked_"/> and <see cref="attacking_"/>.
		/// </summary>
		void GeneratePawnAttacks();
		/// <summary>
		/// Searches for the best move from the specified moves at a certain depth.
		/// </summary>
		/// <param name="moves">Moves to analyze.</param>
		/// <param name="depth">The depth.</param>
		/// <returns>The best move.</returns>
		Move BestMove(const std::deque<Move>& moves, int depth);
		/// <summary>
		/// Calculates the final score of the current position, provided that every player picks the best move.
		/// </summary>
		/// <param name="depth">The depth.</param>
		/// <param name="alpha">The lower bound.</param>
		/// <param name="beta">The upper bound.</param>
		/// <returns>The score.</returns>
		int NegaMax(int depth, int alpha, int beta);
		/// <summary>
		/// Sorts the specified moves by how likely they are to cause a beta-cutoff.
		/// </summary>
		/// <param name="moves">The moves.</param>
		void OrderMoves(std::deque<Move>& moves);
	};

	inline const std::deque<Move>& Position::ListMoves()
	{
		Analyze();
		return possible_moves_;
	}

	inline bool Position::IsCheck()
	{
		Analyze();
		return check_;
	}

	inline bool Position::IsCheckMate()
	{
		Analyze();
		return checkmate_;
	}

	inline bool Position::IsDraw()
	{
		Analyze();
		return stalemate_ || insuf_material_ || threefold_repetition_;
	}

	inline bool Position::IsGameOver()
	{
		Analyze();
		return checkmate_ || IsDraw();
	}

	inline uint64_t Position::GetHash() const
	{
		return hash_;
	}
}
