#include "globals.h"
#include "bitboard.h"

namespace chesstoadpp
{
	Bitboard BitboardTools::ShiftV(const Bitboard bb, const int ranks)
	{
		return ranks > 0 ? ShiftUp(bb, ranks) : ShiftDown(bb, -ranks);
	}

	Bitboard BitboardTools::ShiftH(const Bitboard bb, const int files)
	{
		return files > 0 ? ShiftRight(bb, files) : ShiftLeft(bb, -files);
	}
}
