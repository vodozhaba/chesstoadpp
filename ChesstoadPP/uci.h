#pragma once

#include "globals.h"
#include "position.h"

namespace chesstoadpp
{
	class Uci
	{
		friend class Position;
	public:
		static Uci uci_singleton_;
		/// <summary>
		/// Initializes a new instance of the <see cref="Uci"/> class.
		/// </summary>
		/// <param name="is">The input stream.</param>
		/// <param name="os">The output to stream.</param>
		Uci(std::istream& is, std::ostream& os);
		/// <summary>
		/// Executes UCI commands until the quit command is received from the input stream.
		/// </summary>
		void Run();
	private:
		/// <summary>
		/// Set iff the next command from the GUI should be processed.
		/// </summary>
		bool is_running_;
		/// <summary>
		/// Set iff the engine is currently uninitialized and should be intialized upon receiving the next command from the GUI.
		/// </summary>
		bool initialize_on_next_cmd_;
		/// <summary>
		/// Set iff the debug mode is currently enabled.
		/// </summary>
		bool debug_;
		/// <summary>
		/// Set iff the best move should not be passed to the GUI upon finishing the current analysis.
		/// </summary>
		bool discard_best_move_;
		/// <summary>
		/// Set iff the next branch should be analyzed.
		/// </summary>
		std::atomic_bool is_analyzing_;
		/// <summary>
		/// The input stream reference.
		/// </summary>
		std::istream& is_;
		/// <summary>
		/// The output stream reference.
		/// </summary>
		std::ostream& os_;
		/// <summary>
		/// The currently analyzed position;
		/// </summary>
		std::unique_ptr<Position> pos_;
		/// <summary>
		/// The current analysis task.
		/// </summary>
		std::unique_ptr<std::future<Move>> analysis_;
		/// <summary>
		/// Prints the UCI header.
		/// </summary>
		void CmdUci();
		/// <summary>
		/// Turns the debug mode on/off.
		/// </summary>
		/// <param name="options">The options.</param>
		void CmdDebug(std::queue<std::string>& options);
		/// <summary>
		/// Reports when the engine is ready to receive commands.
		/// </summary>
		void CmdIsReady() const;
		/// <summary>
		/// Sets a specified internal option.
		/// </summary>
		/// <param name="options">The options.</param>
		void CmdSetOption(std::queue<std::string>& options);
		/// <summary>
		/// Reports the registration status of the engine.
		/// </summary>
		/// <param name="options">The options.</param>
		void CmdRegister(std::queue<std::string>& options) const;
		/// <summary>
		/// Tells the engine that the next position will be from a new game.
		/// </summary>
		void CmdUciNewGame();
		/// <summary>
		/// Sets the analyzed position.
		/// </summary>
		/// <param name="options">The options.</param>
		void CmdPosition(std::queue<std::string>& options);
		/// <summary>
		/// Starts asynchronous analysis.
		/// </summary>
		/// <param name="options">The options.</param>
		void CmdGo(std::queue<std::string>& options);
		/// <summary>
		/// Stops asynchronous analysis.
		/// </summary>
		void CmdStop();
		/// <summary>
		/// Tells the engine that the opponent made the predicted move.
		/// </summary>
		void CmdPonderHit();
		/// <summary>
		/// Stops the engine.
		/// </summary>
		void CmdQuit();
		/// <summary>
		/// Stops the current analysis task and discards the results (if any), starts a new analysis task.
		/// </summary>
		/// <param name="moves">Moves to search.</param>
		/// <param name="depth">Max depth to search to.</param>
		void StartAnalysis(const std::deque<Move>& moves, int depth);
		/// <summary>
		/// Stops the current analysis and passes the results to the GUI, unless <see cref="discard_best_move_"/> is set.
		/// </summary>
		void StopAnalysis();
		/// <summary>
		/// Passes the best move to the GUI.
		/// </summary>
		/// <param name="move">The best move.</param>
		void BestMove(Move move) const;
	};
}
