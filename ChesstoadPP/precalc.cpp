#include "globals.h"
#include "precalc.h"

namespace chesstoadpp
{
	Bitboard PrecalcBitboards::rays_[8][64];
	Bitboard PrecalcBitboards::knight_attacks_[64];
	Bitboard PrecalcBitboards::king_attacks_[64];
	Bitboard PrecalcBitboards::pawn_attacks_[64];

	void PrecalcBitboards::Init()
	{
		const auto file_a = 0x0101010101010101ULL;
		const auto rank_1 = 0x00000000000000FFULL;
		const auto diag = 0x8040201008040201ULL;
		const auto anti_diag = 0x0102040810204080ULL;
		const auto knight_c3 = 0x0000000a1100110aULL;
		const auto king_b2 = 0x0000000000070507ULL;
		const auto pawn_b1 = 0x0000000000000005ULL;
		for (auto rank = 0; rank < 8; ++rank)
		{
			for (auto file = 0; file < 8; ++file)
			{
				const auto square = SquareTools::CoordConstruct(rank, file);
				rays_[kNoWe][square] = BitboardTools::ShiftUp(BitboardTools::ShiftLeft(anti_diag, 8 - file), rank + 1);
				rays_[kNo][square] = BitboardTools::ShiftUp(BitboardTools::ShiftRight(file_a, file), rank + 1);
				rays_[kNoEa][square] = BitboardTools::ShiftUp(BitboardTools::ShiftRight(diag, file + 1), rank + 1);
				rays_[kEa][square] = BitboardTools::ShiftUp(BitboardTools::ShiftRight(rank_1, file + 1), rank);
				rays_[kSoEa][square] = BitboardTools::ShiftDown(BitboardTools::ShiftRight(anti_diag, file + 1),
				                                                8 - rank);
				rays_[kSo][square] = BitboardTools::ShiftDown(BitboardTools::ShiftRight(file_a, file), 8 - rank);
				rays_[kSoWe][square] = BitboardTools::ShiftDown(BitboardTools::ShiftLeft(diag, 8 - file), 8 - rank);
				rays_[kWe][square] = BitboardTools::ShiftUp(BitboardTools::ShiftLeft(rank_1, 8 - file), rank);
				knight_attacks_[square] = BitboardTools::ShiftV(BitboardTools::ShiftH(knight_c3, file - 2), rank - 2);
				king_attacks_[square] = BitboardTools::ShiftV(BitboardTools::ShiftH(king_b2, file - 1), rank - 1);
				pawn_attacks_[square] = BitboardTools::ShiftV(BitboardTools::ShiftH(pawn_b1, file - 1), rank);
			}
		}
	}

	Bitboard PrecalcBitboards::RookAttacks(const Square square, const Bitboard occupancy)
	{
		auto attacks = BitboardTools::kEmpty;
		for (auto dir = kNo; dir <= kEa; dir += 2)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 0x8000000000000000ULL;
			const auto first_blocker = BitboardTools::BitScanForward(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		for (auto dir = kSo; dir <= kWe; dir += 2)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 1ULL;
			const auto first_blocker = BitboardTools::BitScanReverse(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		return attacks;
	}

	Bitboard PrecalcBitboards::BishopAttacks(Square square, Bitboard occupancy)
	{
		auto attacks = BitboardTools::kEmpty;
		for (auto dir = kNoWe; dir <= kNoEa; dir += 2)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 0x8000000000000000ULL;
			const auto first_blocker = BitboardTools::BitScanForward(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		for (auto dir = kSoEa; dir <= kSoWe; dir += 2)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 1ULL;
			const auto first_blocker = BitboardTools::BitScanReverse(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		return attacks;
	}

	Bitboard PrecalcBitboards::QueenAttacks(Square square, Bitboard occupancy)
	{
		auto attacks = BitboardTools::kEmpty;
		for (auto dir = kNoWe; dir <= kEa; ++dir)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 0x8000000000000000ULL;
			const auto first_blocker = BitboardTools::BitScanForward(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		for (auto dir = kSoEa; dir <= kWe; ++dir)
		{
			const auto ray = rays_[dir][square];
			const auto blockers = (ray & occupancy) | 1ULL;
			const auto first_blocker = BitboardTools::BitScanReverse(blockers);
			const auto blocked = rays_[dir][first_blocker];
			attacks |= ray ^ blocked;
		}
		return attacks;
	}
}
