#pragma once
#include "globals.h"
#include "square.h"
#include "piece.h"

namespace chesstoadpp
{
	typedef uint16_t Move;

	class MoveTools
	{
	public:
		/// <summary>
		/// The null move.
		/// </summary>
		static constexpr Move kInvalid = 0xEFFF;
		/// <summary>
		/// Constructs a move from source, destination and optional piece to promote to.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="dest">The destination.</param>
		/// <param name="promotion">The piece to promote to.</param>
		/// <returns>The resulting move</returns>
		static Move SquareConstruct(Square source, Square dest, Piece promotion);
		/// <summary>
		/// Constructs a move from a UCI-style string.
		/// </summary>
		/// <param name="s">The string.</param>
		/// <returns>The resulting move if the string is valid, null move otherwise.</returns>
		static Move StrConstruct(const std::string& s);
		/// <summary>
		/// Generates a UCI-style string for the specified move.
		/// </summary>
		/// <param name="move">The move.</param>
		/// <returns>The resulting string.</returns>
		static std::string ToString(Move move);
		/// <summary>
		/// Gets the specified move's source square.
		/// </summary>
		/// <param name="move">The move.</param>
		/// <returns>The source square.</returns>
		static Square Source(Move move);
		/// <summary>
		/// Gets the specified move's destination square.
		/// </summary>
		/// <param name="move">The move.</param>
		/// <returns>The destination square.</returns>
		static Square Dest(Move move);
		/// <summary>
		/// Gets the specified move's piece to promote to.
		/// </summary>
		/// <param name="move">The move.</param>
		/// <returns>The piece to promote to.</returns>
		static Piece Promotion(Move move);
	};

	inline Move MoveTools::SquareConstruct(const Square source, const Square dest, const Piece promotion)
	{
		if (source == SquareTools::kInvalid || dest == SquareTools::kInvalid)
			return kInvalid;
		return ((promotion & 0xE) << 12) | (dest << 6) | source;
	}

	inline Square MoveTools::Source(const Move move)
	{
		return move & 0x3F;
	}

	inline Square MoveTools::Dest(const Move move)
	{
		return (move >> 6) & 0x3F;
	}

	inline Piece MoveTools::Promotion(const Move move)
	{
		return move >> 12;
	}
}
