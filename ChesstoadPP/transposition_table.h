#pragma once
#include "globals.h"
#include "position.h"

namespace chesstoadpp
{
	struct TranspositionTableEntry
	{
		/// <summary>
		/// What the stored score means
		/// </summary>
		enum
		{
			kInvalid,
			kLowerBound,
			kExact,
			kUpperBound
		} flag;

		/// <summary>
		/// The depth of the previous analysis
		/// </summary>
		int depth;
		/// <summary>
		/// The corresponding position's score or lower/upper bound
		/// </summary>
		int score;
		/// <summary>
		/// The best move determined by the previous analysis
		/// </summary>
		Move hash_move;
	};

	class TranspositionTable
	{
	public:
		static std::unique_ptr<TranspositionTable> transposition_table_singleton_;
		std::uint64_t pieces_[12][64];
		std::uint64_t color_to_move_[2];
		std::uint64_t castling_rights_configs_[16];
		std::uint64_t en_passant_files_[8];
		/// <summary>
		/// Initializes the application-wide transposition table.
		/// </summary>
		static void Init();
		/// <summary>
		/// Initializes a new instance of the <see cref="TranspositionTable"/> class.
		/// </summary>
		explicit TranspositionTable();
		/// <summary>
		/// Changes the memory limit for this instance.
		/// </summary>
		/// <param name="memory">The new memory limit.</param>
		void Resize(size_t memory);
		/// <summary>
		/// Calculates the specified position's Zobrist hash.
		/// </summary>
		/// <param name="pos">The position.</param>
		/// <returns>The hash.</returns>
		uint64_t Hash(const Position& pos);
		/// <summary>
		/// Requests storing a specified entry. Whether the entry will actually be stored depends on the implemented replacement strategy.
		/// </summary>
		/// <param name="hash">The hash of the corresponsing position.</param>
		/// <param name="tt_entry">The transposition table entry.</param>
		void Store(uint64_t hash, const TranspositionTableEntry& tt_entry);
		/// <summary>
		/// Looks for an entry corresponsing to a specified hash.
		/// </summary>
		/// <param name="hash">The hash.</param>
		/// <returns>If found, returns the corresponding entry, otherwise an invalid entry.</returns>
		TranspositionTableEntry Lookup(uint64_t hash);
	private:
		/// <summary>
		/// Returns a pseudo-random 64-bit number.
		/// </summary>
		/// <returns></returns>
		std::uint64_t Prng();
		std::uint64_t prng_state_[2];
		std::vector<std::pair<uint64_t, TranspositionTableEntry>> entries_;
	};
}
