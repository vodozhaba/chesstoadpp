#include "globals.h"
#include "position.h"
#include "precalc.h"
#include "transposition_table.h"
#include "magic_bitboards.h"

namespace chesstoadpp
{
	const Move Position::kWhiteShortCastling = MoveTools::StrConstruct("e1g1");
	const Move Position::kWhiteLongCastling = MoveTools::StrConstruct("e1c1");
	const Move Position::kBlackShortCastling = MoveTools::StrConstruct("e8g8");
	const Move Position::kBlackLongCastling = MoveTools::StrConstruct("e8c8");

	Position::Position(const std::string& fen) : castling_(0), analyzed_(false)
	{
		std::stringstream fen_ss(fen);
		std::string token;
		fen_ss >> token;
		std::stringstream piece_placement_ss(token);
		std::string fen_row;
		std::memset(bitboards_, 0, sizeof(bitboards_));
		for (auto rank = 7; std::getline(piece_placement_ss, fen_row, '/'); --rank)
		{
			auto file = 0;
			for (auto c : fen_row)
			{
				const auto square = SquareTools::CoordConstruct(rank, file);
				const auto bb = BitboardTools::SingleSquare(square);
				const auto piece = PieceTools::CharConstruct(c);
				if (piece != PieceTools::kNone)
				{
					bitboards_[piece] |= bb;
					++file;
				}
				else if (c > '0' && c < '9')
				{
					file += c - '0';
				}
				if (file & 0xF8)
					break;
			}
		}
		fen_ss >> token;
		current_player_ = token[0] == 'b' ? PieceTools::kBlack : PieceTools::kWhite;
		opponent_ = !current_player_;
		fen_ss >> token;
		for (auto c : token)
		{
			switch (c)
			{
			case 'K':
				castling_ |= 1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kWhite);
				break;
			case 'Q':
				castling_ |= 1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kWhite);
				break;
			case 'k':
				castling_ |= 1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kBlack);
				break;
			case 'q':
				castling_ |= 1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kBlack);
				break;
			}
		}
		fen_ss >> token;
		en_passant_ = SquareTools::StrConstruct(token);
		fen_ss >> last_reversible_moves_;
		fen_ss >> current_move_;
		hash_ = TranspositionTable::transposition_table_singleton_->Hash(*this);
		hash_history_.push_back(hash_);
	}

	Position::Position(const Position& other, const bool preserve_analysis)
	{
		Init(other, preserve_analysis);
	}

	void Position::Make(const Move move)
	{
		static const auto kWhiteShortRook = SquareTools::kH1;
		static const auto kWhiteLongRook = SquareTools::kA1;
		static const auto kBlackShortRook = SquareTools::kH8;
		static const auto kBlackLongRook = SquareTools::kA8;

		auto reset_reversible = false;
		auto reset_en_passant = true;
		auto piece = PieceTools::kNone;
		const auto source = MoveTools::Source(move);
		const auto source_bb = BitboardTools::SingleSquare(source);
		const auto dest = MoveTools::Dest(move);
		const auto dest_bb = BitboardTools::SingleSquare(dest);

		hash_ ^= TranspositionTable::transposition_table_singleton_->castling_rights_configs_[castling_];

		for (auto i = 0; i < 12; ++i)
		{
			if (bitboards_[i] & source_bb)
			{
				piece = i;
				bitboards_[i] &= ~source_bb;
				switch(dest)
				{
				case kWhiteShortRook:
					castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kWhite));
					break;
				case kWhiteLongRook:
					castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kWhite));
					break;
				case kBlackShortRook:
					castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kBlack));
					break;
				case kBlackLongRook:
					castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kBlack));
					break;
				}
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[i][source];
				break;
			}
		}

		if (PieceTools::Type(piece) == PieceTools::kPawn)
		{
			const auto promotion = MoveTools::Promotion(move);
			reset_reversible = true;
			reset_en_passant = HandlePawnMove(source, dest, promotion, piece);
		}
		else if (PieceTools::Type(piece) == PieceTools::kKing)
			HandleKingMove(PieceTools::Color(piece), move);
		else if (PieceTools::Type(piece) == PieceTools::kRook)
			HandleRookMove(source);

		const auto mask = ~dest_bb;
		for (auto i = 0; i < 12; ++i)
		{
			auto& bitboard = bitboards_[i];
			if (bitboard & dest_bb)
			{
				reset_reversible = false;
				bitboard &= mask;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[i][dest];
				break;
			}
		}
		bitboards_[piece] |= dest_bb;
		hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[piece][dest];

		current_player_ = opponent_;
		opponent_ = !opponent_;
		hash_ ^= TranspositionTable::transposition_table_singleton_->color_to_move_[PieceTools::kBlack];
		if (current_player_ == PieceTools::kWhite)
		{
			++current_move_;
			++last_reversible_moves_;
		}
		if (reset_reversible)
			last_reversible_moves_ = 0;
		if (en_passant_ != SquareTools::kInvalid && reset_en_passant)
		{
			hash_ ^= TranspositionTable::transposition_table_singleton_->en_passant_files_[SquareTools::File(
				en_passant_)];
			en_passant_ = SquareTools::kInvalid;
		}

		hash_ ^= TranspositionTable::transposition_table_singleton_->castling_rights_configs_[castling_];
		hash_history_.push_back(hash_);
		analyzed_ = false;
	}

	Position& Position::operator=(const Position& other)
	{
		Init(other, true);
		return *this;
	}

	void Position::Init(const Position& other, const bool preserve_analysis)
	{
		for (auto i = 0; i < 12; ++i)
			bitboards_[i] = other.bitboards_[i];
		current_player_ = other.current_player_;
		opponent_ = other.opponent_;
		castling_ = other.castling_;
		en_passant_ = other.en_passant_;
		last_reversible_moves_ = other.last_reversible_moves_;
		current_move_ = other.current_move_;
		hash_ = other.hash_;
		hash_history_.reserve(other.hash_history_.size() + 1);
		std::copy(other.hash_history_.begin(), other.hash_history_.end(), std::back_inserter(hash_history_));
		analyzed_ = false;
		if (!preserve_analysis)
			return;
		en_passant_bb_ = other.en_passant_bb_;
		color_based_occupancy_[PieceTools::kWhite] = other.color_based_occupancy_[PieceTools::kWhite];
		color_based_occupancy_[PieceTools::kBlack] = other.color_based_occupancy_[PieceTools::kBlack];
		current_player_occupancy_ = other.current_player_occupancy_;
		opponent_occupancy_ = other.opponent_occupancy_;
		occupancy_ = other.occupancy_;
		attacked_ = other.attacked_;
		attacking_ = other.attacking_;
		possible_moves_ = other.possible_moves_;
		check_ = other.check_;
		checkmate_ = other.checkmate_;
		stalemate_ = other.stalemate_;
		threefold_repetition_ = other.threefold_repetition_;
		middle_game_ = other.middle_game_;
		endgame_ = other.endgame_;
		insuf_material_ = other.insuf_material_;
		analyzed_ = other.analyzed_;
	}


	inline bool Position::HandlePawnMove(const Square source, const Square dest, const Piece promotion, Piece& piece)
	{
		int r_d, r_s;
		if (dest == en_passant_)
		{
			const auto target = SquareTools::CoordConstruct(SquareTools::Rank(source), SquareTools::File(dest));
			const auto target_bb = BitboardTools::SingleSquare(target);
			const auto target_mask = ~target_bb;
			const auto target_pawn = PieceTools::kPawn | opponent_;
			auto& bitboard = bitboards_[target_pawn];
			hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[target_pawn][target];
			bitboard &= target_mask;
		}
		else if (std::abs((r_d = SquareTools::Rank(dest)) - (r_s = SquareTools::Rank(source))) == 2)
		{
			const auto ep_file = SquareTools::File(source);
			en_passant_ = SquareTools::CoordConstruct((r_d + r_s) / 2, ep_file);
			hash_ ^= TranspositionTable::transposition_table_singleton_->en_passant_files_[ep_file];
			return false;
		}
		if (promotion != PieceTools::kNone)
		{
			piece = promotion | current_player_;
		}
		return true;
	}

	inline void Position::HandleKingMove(const Color color, const Move move)
	{
		static const auto kWhiteShortRookSource = SquareTools::kH1;
		static const auto kWhiteShortRookMask = 0xFFFFFFFFFFFFFF7FULL;
		static const auto kWhiteLongRookSource = SquareTools::kA1;
		static const auto kWhiteLongRookMask = 0xFFFFFFFFFFFFFFFEULL;
		static const auto kBlackShortRookSource = SquareTools::kH8;
		static const auto kBlackShortRookMask = 0x7FFFFFFFFFFFFFFFULL;
		static const auto kBlackLongRookSource = SquareTools::kA8;
		static const auto kBlackLongRookMask = 0xFEFFFFFFFFFFFFFFULL;
		static const auto kWhiteShortRookDest = SquareTools::kF1;
		static const auto kWhiteShortRookBb = 0x0000000000000020ULL;
		static const auto kWhiteLongRookDest = SquareTools::kD1;
		static const auto kWhiteLongRookBb = 0x0000000000000008ULL;
		static const auto kBlackShortRookDest = SquareTools::kF8;
		static const auto kBlackShortRookBb = 0x2000000000000000ULL;
		static const auto kBlackLongRookDest = SquareTools::kD8;
		static const auto kBlackLongRookBb = 0x0800000000000000ULL;
		if (!castling_)
			return;
		if (color == PieceTools::kWhite)
		{
			castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kWhite));
			castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kWhite));
			const auto rook = PieceTools::kRook | PieceTools::kWhite;
			if (move == kWhiteShortCastling)
			{
				bitboards_[rook] &= kWhiteShortRookMask;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kWhiteShortRookSource];
				bitboards_[rook] |= kWhiteShortRookBb;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kWhiteShortRookDest];
			}
			else if (move == kWhiteLongCastling)
			{
				bitboards_[rook] &= kWhiteLongRookMask;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kWhiteLongRookSource];
				bitboards_[rook] |= kWhiteLongRookBb;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kWhiteLongRookDest];
			}
		}
		else
		{
			castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kBlack));
			castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kBlack));
			const auto rook = PieceTools::kRook | PieceTools::kBlack;
			if (move == kBlackShortCastling)
			{
				bitboards_[rook] &= kBlackShortRookMask;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kBlackShortRookSource];
				bitboards_[rook] |= kBlackShortRookBb;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kBlackShortRookDest];
			}
			else if (move == kBlackLongCastling)
			{
				bitboards_[rook] &= kBlackLongRookMask;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kBlackLongRookSource];
				bitboards_[rook] |= kBlackLongRookBb;
				hash_ ^= TranspositionTable::transposition_table_singleton_->pieces_[rook][kBlackLongRookDest];
			}
		}
	}

	inline void Position::HandleRookMove(const Square source)
	{
		static const auto kWhiteShortRook = SquareTools::kH1;
		static const auto kWhiteLongRook = SquareTools::kA1;
		static const auto kBlackShortRook = SquareTools::kH8;
		static const auto kBlackLongRook = SquareTools::kA8;
		if (!castling_)
			return;
		if (source == kWhiteShortRook)
			castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kWhite));
		else if (source == kWhiteLongRook)
			castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kWhite));
		else if (source == kBlackShortRook)
			castling_ &= ~(1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kBlack));
		else if (source == kBlackLongRook)
			castling_ &= ~(1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kBlack));
	}

	void Position::Analyze(const bool generate_moves)
	{
		if (analyzed_)
			return;
		color_based_occupancy_[PieceTools::kWhite] = BitboardTools::kEmpty;
		color_based_occupancy_[PieceTools::kBlack] = BitboardTools::kEmpty;
		for (auto piece = 0; piece < 12; ++piece)
		{
			color_based_occupancy_[piece & 1] |= bitboards_[piece];
		}
		current_player_occupancy_ = color_based_occupancy_[current_player_];
		opponent_occupancy_ = color_based_occupancy_[!current_player_];
		occupancy_ = current_player_occupancy_ | opponent_occupancy_;
		en_passant_bb_ = (en_passant_ == SquareTools::kInvalid)
			                 ? BitboardTools::kEmpty
			                 : BitboardTools::SingleSquare(en_passant_);
		GenerateAttacks();
		const auto king = bitboards_[PieceTools::kKing | current_player_];
		check_ = king & attacked_;
		if (generate_moves)
		{
			GenerateMoves();
			if (possible_moves_.empty())
			{
				if (check_)
					checkmate_ = true;
				else
					stalemate_ = true;
			}
		}
		const auto minor_pieces = bitboards_[PieceTools::kKnight | PieceTools::kWhite] | bitboards_[PieceTools::kKnight
			| PieceTools::kBlack] | bitboards_[PieceTools::kBishop | PieceTools::kWhite] | bitboards_[PieceTools::
			kBishop | PieceTools::kBlack];
		const auto kings = king | bitboards_[PieceTools::kKing | opponent_];
		if (occupancy_ == (minor_pieces | kings) && BitboardTools::Count(minor_pieces) < 2)
		{
			insuf_material_ = true;
		}
		auto repetitions = 0;
		for (auto it = hash_history_.begin(); it != hash_history_.end(); ++it)
		{
			if (*it != hash_ || ++repetitions != 2)
				continue;
			threefold_repetition_ = true;
			break;
		}
		const auto white_queens = bitboards_[PieceTools::kQueen | PieceTools::kWhite];
		const auto black_queens = bitboards_[PieceTools::kQueen | PieceTools::kBlack];
		if (white_queens == BitboardTools::kEmpty && black_queens == BitboardTools::kEmpty)
		{
			endgame_ = true;
		}
		else
		{
			const auto white_rooks = bitboards_[PieceTools::kRook | PieceTools::kWhite];
			const auto white_minor = bitboards_[PieceTools::kKnight | PieceTools::kWhite] | bitboards_[PieceTools::
				kBishop | PieceTools::kWhite];
			const auto black_rooks = bitboards_[PieceTools::kRook | PieceTools::kBlack];
			const auto black_minor = bitboards_[PieceTools::kKnight | PieceTools::kBlack] | bitboards_[PieceTools::
				kBishop | PieceTools::kBlack];
			if (white_queens)
				if (white_rooks == BitboardTools::kEmpty && BitboardTools::Count(white_minor) < 2)
					endgame_ = true;
				else
					middle_game_ = true;
			else if (black_queens)
				if (black_rooks == BitboardTools::kEmpty && BitboardTools::Count(black_minor) < 2)
					endgame_ = true;
				else
					middle_game_ = true;
			else
				middle_game_ = true;
		}
		analyzed_ = true;
	}

	inline void Position::GenerateMoves()
	{
		possible_moves_.clear();
		GenerateRookMoves();
		GenerateKnightMoves();
		GenerateBishopMoves();
		GenerateQueenMoves();
		GenerateKingMoves();
		GeneratePawnMoves();
		const auto king = PieceTools::kKing | current_player_;
		auto it = possible_moves_.begin();
		while (it != possible_moves_.end())
		{
			Position post_move(*this, false);
			auto move = *it;
			post_move.Make(move);
			post_move.Analyze(false);
			const auto king_bb = post_move.bitboards_[king];
			if (post_move.attacking_ & king_bb)
				it = possible_moves_.erase(it);
			else
				++it;
		}
	}

	void Position::GenerateRookMoves()
	{
		auto rooks = bitboards_[PieceTools::kRook | current_player_];
		auto offset = 0;
		while (rooks)
		{
			auto square = BitboardTools::BitScanForward(rooks);
			if (square != 63)
			{
				rooks >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				rooks = BitboardTools::kEmpty;
			}
			const auto dests = MagicBitboards::RookAttacks(square, occupancy_) & ~current_player_occupancy_;
			AddMoves(square, dests);
		}
	}

	void Position::GenerateKnightMoves()
	{
		auto knights = bitboards_[PieceTools::kKnight | current_player_];
		auto offset = 0;
		while (knights)
		{
			auto square = BitboardTools::BitScanForward(knights);
			if (square != 63)
			{
				knights >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				knights = BitboardTools::kEmpty;
			}
			const auto dests = PrecalcBitboards::KnightAttacks(square) & ~current_player_occupancy_;
			AddMoves(square, dests);
		}
	}

	void Position::GenerateBishopMoves()
	{
		auto bishops = bitboards_[PieceTools::kBishop | current_player_];
		auto offset = 0;
		while (bishops)
		{
			auto square = BitboardTools::BitScanForward(bishops);
			if (square != 63)
			{
				bishops >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				bishops = BitboardTools::kEmpty;
			}
			const auto dests = MagicBitboards::BishopAttacks(square, occupancy_) & ~current_player_occupancy_;
			AddMoves(square, dests);
		}
	}

	void Position::GenerateQueenMoves()
	{
		auto queens = bitboards_[PieceTools::kQueen | current_player_];
		auto offset = 0;
		while (queens)
		{
			auto square = BitboardTools::BitScanForward(queens);
			if (square != 63)
			{
				queens >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				queens = BitboardTools::kEmpty;
			}
			const auto dests = MagicBitboards::QueenAttacks(square, occupancy_) & ~current_player_occupancy_;
			AddMoves(square, dests);
		}
	}

	void Position::GenerateKingMoves()
	{
		static const auto kWhiteShortCastlingEmptySquares = 0x0000000000000060ULL;
		static const auto kWhiteLongCastlingEmptySquares = 0x000000000000000EULL;
		static const auto kBlackShortCastlingEmptySquares = 0x6000000000000000ULL;
		static const auto kBlackLongCastlingEmptySquares = 0x0E00000000000000ULL;
		static const auto kWhiteShortCastlingSafeSquares = 0x0000000000000070ULL;
		static const auto kWhiteLongCastlingSafeSquares = 0x000000000000001CULL;
		static const auto kBlackShortCastlingSafeSquares = 0x7000000000000000ULL;
		static const auto kBlackLongCastlingSafeSquares = 0x1C00000000000000ULL;
		const auto source = BitboardTools::BitScanForward(bitboards_[PieceTools::kKing | current_player_]);
		const auto dests = PrecalcBitboards::KingAttacks(source) & ~current_player_occupancy_;
		AddMoves(source, dests);
		if (current_player_ == PieceTools::kWhite)
		{
			if (!((occupancy_ & kWhiteShortCastlingEmptySquares) || (attacked_ & kWhiteShortCastlingSafeSquares)) && (
				castling_ & (1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kWhite))))
				possible_moves_.push_back(kWhiteShortCastling);
			if (!((occupancy_ & kWhiteLongCastlingEmptySquares) || (attacked_ & kWhiteLongCastlingSafeSquares)) && (
				castling_ & (1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kWhite))))
				possible_moves_.push_back(kWhiteLongCastling);
		}
		else
		{
			if (!((occupancy_ & kBlackShortCastlingEmptySquares) || (attacked_ & kBlackShortCastlingSafeSquares)) && (
				castling_ & (1 << (CastlingPermissionsTools::kShortCastling | PieceTools::kBlack))))
				possible_moves_.push_back(kBlackShortCastling);
			if (!((occupancy_ & kBlackLongCastlingEmptySquares) || (attacked_ & kBlackLongCastlingSafeSquares)) && (
				castling_ & (1 << (CastlingPermissionsTools::kLongCastling | PieceTools::kBlack))))
				possible_moves_.push_back(kBlackLongCastling);
		}
	}

	void Position::GeneratePawnMoves()
	{
		auto pawns = bitboards_[PieceTools::kPawn | current_player_];
		const auto attackable = opponent_occupancy_ | en_passant_bb_;
		auto offset = 0;
		while (pawns)
		{
			auto source = BitboardTools::BitScanForward(pawns);
			if (source != 63)
			{
				pawns >>= source + 1;
				source += offset;
				offset = source + 1;
			}
			else
			{
				pawns = BitboardTools::kEmpty;
			}
			const auto dests = PrecalcBitboards::PawnAttacks(source, current_player_) & attackable;
			int pre_promotion_rank, initial_rank, direction;
			if (current_player_ == PieceTools::kWhite)
			{
				pre_promotion_rank = 6;
				initial_rank = 1;
				direction = 1;
			}
			else
			{
				pre_promotion_rank = 1;
				initial_rank = 6;
				direction = -1;
			}
			const auto promote = SquareTools::Rank(source) == pre_promotion_rank;
			if (promote)
			{
				AddMoves(source, dests, PieceTools::kRook);
				AddMoves(source, dests, PieceTools::kKnight);
				AddMoves(source, dests, PieceTools::kBishop);
				AddMoves(source, dests, PieceTools::kQueen);
			}
			else
			{
				AddMoves(source, dests);
			}
			auto dest = SquareTools::Shift(source, direction, 0);
			if (!(BitboardTools::SingleSquare(dest) & occupancy_))
			{
				if (promote)
				{
					possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kRook));
					possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kKnight));
					possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kBishop));
					possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kQueen));
					return;
				}
				possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kNone));
				if (SquareTools::Rank(source) == initial_rank)
				{
					dest = SquareTools::Shift(dest, direction, 0);
					if (!(BitboardTools::SingleSquare(dest) & occupancy_))
						possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, PieceTools::kNone));
				}
			}
		}
	}

	void Position::AddMoves(const Square source, Bitboard dests, const Piece promotion)
	{
		auto offset = 0;
		while (dests)
		{
			auto dest = BitboardTools::BitScanForward(dests);
			if (dest != 63)
			{
				dests >>= dest + 1;
				dest += offset;
				offset = dest + 1;
			}
			else
			{
				dests = BitboardTools::kEmpty;
			}
			possible_moves_.push_back(MoveTools::SquareConstruct(source, dest, promotion));
		}
	}

	inline void Position::GenerateAttacks()
	{
		attacking_ = BitboardTools::kEmpty;
		attacked_ = BitboardTools::kEmpty;
		GenerateRookAttacks();
		GenerateKnightAttacks();
		GenerateBishopAttacks();
		GenerateQueenAttacks();
		GenerateKingAttacks();
		GeneratePawnAttacks();
	}

	void Position::GenerateRookAttacks()
	{
		auto rooks = bitboards_[PieceTools::kRook | current_player_];
		auto offset = 0;
		while (rooks)
		{
			auto square = BitboardTools::BitScanForward(rooks);
			if (square != 63)
			{
				rooks >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				rooks = BitboardTools::kEmpty;
			}
			attacking_ |= MagicBitboards::RookAttacks(square, occupancy_);
		}
		rooks = bitboards_[PieceTools::kRook | opponent_];
		offset = 0;
		while (rooks)
		{
			auto square = BitboardTools::BitScanForward(rooks);
			if (square != 63)
			{
				rooks >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				rooks = BitboardTools::kEmpty;
			}
			attacked_ |= MagicBitboards::RookAttacks(square, occupancy_);
		}
	}

	void Position::GenerateKnightAttacks()
	{
		auto knights = bitboards_[PieceTools::kKnight | current_player_];
		auto offset = 0;
		while (knights)
		{
			auto square = BitboardTools::BitScanForward(knights);
			if (square != 63)
			{
				knights >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				knights = BitboardTools::kEmpty;
			}
			attacking_ |= PrecalcBitboards::KnightAttacks(square);
		}
		knights = bitboards_[PieceTools::kKnight | opponent_];
		offset = 0;
		while (knights)
		{
			auto square = BitboardTools::BitScanForward(knights);
			if (square != 63)
			{
				knights >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				knights = BitboardTools::kEmpty;
			}
			attacked_ |= PrecalcBitboards::KnightAttacks(square);
		}
	}

	void Position::GenerateBishopAttacks()
	{
		auto bishops = bitboards_[PieceTools::kBishop | current_player_];
		auto offset = 0;
		while (bishops)
		{
			auto square = BitboardTools::BitScanForward(bishops);
			if (square != 63)
			{
				bishops >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				bishops = BitboardTools::kEmpty;
			}
			attacking_ |= MagicBitboards::BishopAttacks(square, occupancy_);
		}
		bishops = bitboards_[PieceTools::kBishop | opponent_];
		offset = 0;
		while (bishops)
		{
			auto square = BitboardTools::BitScanForward(bishops);
			if (square != 63)
			{
				bishops >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				bishops = BitboardTools::kEmpty;
			}
			attacked_ |= MagicBitboards::BishopAttacks(square, occupancy_);
		}
	}

	void Position::GenerateQueenAttacks()
	{
		auto queens = bitboards_[PieceTools::kQueen | current_player_];
		auto offset = 0;
		while (queens)
		{
			auto square = BitboardTools::BitScanForward(queens);
			if (square != 63)
			{
				queens >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				queens = BitboardTools::kEmpty;
			}
			attacking_ |= MagicBitboards::QueenAttacks(square, occupancy_);
		}
		queens = bitboards_[PieceTools::kQueen | opponent_];
		offset = 0;
		while (queens)
		{
			auto square = BitboardTools::BitScanForward(queens);
			if (square != 63)
			{
				queens >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				queens = BitboardTools::kEmpty;
			}
			attacked_ |= MagicBitboards::QueenAttacks(square, occupancy_);
		}
	}

	void Position::GenerateKingAttacks()
	{
		auto square = BitboardTools::BitScanForward(bitboards_[PieceTools::kKing | current_player_]);
		attacking_ |= PrecalcBitboards::KingAttacks(square);
		square = BitboardTools::BitScanForward(bitboards_[PieceTools::kKing | opponent_]);
		attacked_ |= PrecalcBitboards::KingAttacks(square);
	}

	void Position::GeneratePawnAttacks()
	{
		auto color = current_player_;
		auto pawns = bitboards_[PieceTools::kPawn | color];
		auto offset = 0;
		while (pawns)
		{
			auto square = BitboardTools::BitScanForward(pawns);
			if (square != 63)
			{
				pawns >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				pawns = BitboardTools::kEmpty;
			}
			attacking_ |= PrecalcBitboards::PawnAttacks(square, color);
		}
		color = opponent_;
		pawns = bitboards_[PieceTools::kPawn | color];
		offset = 0;
		while (pawns)
		{
			auto square = BitboardTools::BitScanForward(pawns);
			if (square != 63)
			{
				pawns >>= square + 1;
				square += offset;
				offset = square + 1;
			}
			else
			{
				pawns = BitboardTools::kEmpty;
			}
			attacked_ |= PrecalcBitboards::PawnAttacks(square, color);
		}
	}
}
