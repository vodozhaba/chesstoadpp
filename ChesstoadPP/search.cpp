#include "globals.h"
#include "position.h"
#include "transposition_table.h"
#include "uci.h"

namespace chesstoadpp
{
	Move Position::BestMoveIterative(const std::deque<Move>& moves, const int depth)
	{
		auto best_move = MoveTools::kInvalid;
		for (auto i = 0; i != depth && Uci::uci_singleton_.is_analyzing_; ++i)
		{
			best_move = BestMove(moves, i);
		}
		Uci::uci_singleton_.BestMove(best_move);
		return best_move;
	}

	Move Position::BestMove(const std::deque<Move>& moves, const int depth)
	{
		Analyze();
		auto mutable_moves = moves;
		OrderMoves(mutable_moves);
		auto max = INT_MIN + 1;
		auto best = MoveTools::kInvalid;
		for (auto move : mutable_moves)
		{
			Position post_move(*this, false);
			post_move.Make(move);
			const auto score = -post_move.NegaMax(depth, INT_MIN + 1, INT_MAX);
			if (score > max)
			{
				max = score;
				best = move;
			}
			if (!Uci::uci_singleton_.is_analyzing_)
				break;
		}
		return best;
	}

	int Position::NegaMax(const int depth, int alpha, int beta)
	{
		// Check for draw before using transposition table to avoid missing 50-move rule and threefold repetition
		Analyze();
		if (IsDraw())
			return 0;
		const auto alpha_orig = alpha;
		auto tt_entry = TranspositionTable::transposition_table_singleton_->Lookup(hash_);
		if (tt_entry.flag != TranspositionTableEntry::kInvalid && tt_entry.depth >= depth)
		{
			if (tt_entry.flag == TranspositionTableEntry::kExact)
				return tt_entry.score;
			if (tt_entry.flag == TranspositionTableEntry::kLowerBound)
				alpha = std::max(alpha, tt_entry.score);
			else if (tt_entry.flag == TranspositionTableEntry::kUpperBound)
				beta = std::min(beta, tt_entry.score);
			if (alpha >= beta)
				return tt_entry.score;
		}
		if (depth == 0 || IsGameOver())
			return Evaluate();
		OrderMoves(possible_moves_);
		auto score = INT_MIN + 1;
		auto best_move = MoveTools::kInvalid;
		for (auto move : possible_moves_)
		{
			Position post_move(*this, false);
			post_move.Make(move);
			score = std::max(score, -post_move.NegaMax(depth - 1, -beta, -alpha));
			// closer checkmate is worth more
			if (score > INT_MAX / 2 - 1000 && score <= INT_MAX / 2)
				--score;
			else if (score < -(INT_MAX / 2) + 1000 && score >= -(INT_MAX / 2))
				++score;
			if (score > alpha)
			{
				alpha = score;
				best_move = move;
			}
			if (alpha >= beta || !Uci::uci_singleton_.is_analyzing_)
				break;
		}
		tt_entry.score = score;
		tt_entry.hash_move = best_move;
		if (score < alpha_orig)
			tt_entry.flag = TranspositionTableEntry::kUpperBound;
		else if (score > beta)
			tt_entry.flag = TranspositionTableEntry::kLowerBound;
		else
			tt_entry.flag = TranspositionTableEntry::kExact;
		tt_entry.depth = depth;
		TranspositionTable::transposition_table_singleton_->Store(hash_, tt_entry);
		return score;
	}

	void Position::OrderMoves(std::deque<Move>& moves)
	{
		std::deque<Move> orig;
		std::swap(orig, moves);
		auto hash_move = TranspositionTable::transposition_table_singleton_->Lookup(hash_).hash_move;
		auto it = std::find(orig.begin(), orig.end(), hash_move);
		if (it != orig.end()) // hash move in allowed moves
		{
			orig.erase(it);
			moves.push_back(hash_move);
		}
		const auto king = bitboards_[PieceTools::kKing | opponent_];
		it = orig.begin();
		while (it != orig.end())
		{
			auto move = *it;
			Position post_move(*this, false);
			post_move.Make(move);
			post_move.Analyze(false);
			if (post_move.attacked_ & king) // checks
			{
				moves.push_back(move);
				it = orig.erase(it);
			}
			else
			{
				++it;
			}
		}
		it = orig.begin();
		while (it != orig.end())
		{
			auto move = *it;
			if (BitboardTools::SingleSquare(MoveTools::Dest(move)) & (opponent_occupancy_)) // captures
			{
				moves.push_back(move);
				it = orig.erase(it);
			}
			else
			{
				++it;
			}
		}
		moves.insert(moves.end(), std::make_move_iterator(orig.begin()), std::make_move_iterator(orig.end()));
	}
}
