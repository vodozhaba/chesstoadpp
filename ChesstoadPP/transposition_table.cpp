#include "globals.h"
#include "transposition_table.h"
#include "position.h"

namespace chesstoadpp
{
	void TranspositionTable::Init()
	{
		transposition_table_singleton_ = std::make_unique<TranspositionTable>();
	}

	TranspositionTable::TranspositionTable()
	{
		prng_state_[0] = 0x35C0E91AA04D9DACULL;
		prng_state_[1] = 0x5BE5E73A981A21CCULL;
		for (auto& piece : pieces_)
			for (auto& square : piece)
				square = Prng();
		color_to_move_[0] = 0;
		color_to_move_[1] = Prng();
		for (auto& config : castling_rights_configs_)
			config = Prng();
		for (auto& file : en_passant_files_)
			file = Prng();
		Resize(1024 * 1024);
	}

	void TranspositionTable::Resize(const size_t hash_memory)
	{
		const auto cap = hash_memory / sizeof(std::pair<uint64_t, TranspositionTableEntry>);
		if (hash_memory < entries_.capacity())
		{
			entries_.resize(cap);
			entries_.shrink_to_fit();
		}
		else
		{
			entries_.reserve(cap);
		}
	}

	uint64_t TranspositionTable::Hash(const Position& pos)
	{
		auto ret = 0ULL;
		for (auto piece = 0; piece < 12; ++piece)
		{
			auto bitboard = pos.bitboards_[piece];
			auto offset = 0;
			while (bitboard)
			{
				auto square = BitboardTools::BitScanForward(bitboard);
				if (square != 63)
				{
					bitboard >>= square + 1;
					square += offset;
					offset = square + 1;
				}
				else
				{
					bitboard = BitboardTools::kEmpty;
				}
				ret ^= pieces_[piece][square];
			}
		}
		ret ^= color_to_move_[pos.current_player_];
		ret ^= castling_rights_configs_[pos.castling_];
		const auto ep = pos.en_passant_;
		if (ep != SquareTools::kInvalid)
			ret ^= en_passant_files_[SquareTools::File(ep)];
		return ret;
	}

	void TranspositionTable::Store(const uint64_t hash, const TranspositionTableEntry& tt_entry)
	{
		const auto ix = hash % entries_.capacity();
		if (ix >= entries_.size())
			entries_.resize(ix + 1);
		auto& dest = entries_[hash % entries_.capacity()];
		if (dest.second.flag == TranspositionTableEntry::kInvalid || tt_entry.depth >= dest.second.depth)
			dest = std::make_pair(hash, static_cast<TranspositionTableEntry>(tt_entry));
	}

	TranspositionTableEntry TranspositionTable::Lookup(const uint64_t hash)
	{
		TranspositionTableEntry invalid{};
		invalid.flag = TranspositionTableEntry::kInvalid;
		const auto ix = hash % entries_.capacity();
		if (ix >= entries_.size())
			return invalid;
		auto ret = entries_[ix];
		if (ret.first == hash)
			return ret.second;
		return invalid;
	}

	std::uint64_t TranspositionTable::Prng()
	{
		auto x = prng_state_[0];
		const auto y = prng_state_[1];
		prng_state_[0] = y;
		x ^= x << 23;
		prng_state_[1] = x ^ y ^ (x >> 17) ^ (y << 26);
		return prng_state_[1] + y;
	}

	/// <summary>
	/// The transposition table singleton{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
	/// </summary>
	std::unique_ptr<TranspositionTable> TranspositionTable::transposition_table_singleton_;
}
