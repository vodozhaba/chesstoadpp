#include "globals.h"
#include "magic_bitboards.h"
#include "precalc.h"

namespace chesstoadpp
{
	const uint64_t MagicBitboards::kRookMagics[64] = {
		0xA8002C000108020ULL,
		0x6C00049B0002001ULL,
		0x100200010090040ULL,
		0x2480041000800801ULL,
		0x280028004000800ULL,
		0x900410008040022ULL,
		0x280020001001080ULL,
		0x2880002041000080ULL,
		0xA000800080400034ULL,
		0x4808020004000ULL,
		0x2290802004801000ULL,
		0x411000D00100020ULL,
		0x402800800040080ULL,
		0xB000401004208ULL,
		0x2409000100040200ULL,
		0x1002100004082ULL,
		0x22878001E24000ULL,
		0x1090810021004010ULL,
		0x801030040200012ULL,
		0x500808008001000ULL,
		0xA08018014000880ULL,
		0x8000808004000200ULL,
		0x201008080010200ULL,
		0x801020000441091ULL,
		0x800080204005ULL,
		0x1040200040100048ULL,
		0x120200402082ULL,
		0xD14880480100080ULL,
		0x12040280080080ULL,
		0x100040080020080ULL,
		0x9020010080800200ULL,
		0x813241200148449ULL,
		0x491604001800080ULL,
		0x100401000402001ULL,
		0x4820010021001040ULL,
		0x400402202000812ULL,
		0x209009005000802ULL,
		0x810800601800400ULL,
		0x4301083214000150ULL,
		0x204026458E001401ULL,
		0x40204000808000ULL,
		0x8001008040010020ULL,
		0x8410820820420010ULL,
		0x1003001000090020ULL,
		0x804040008008080ULL,
		0x12000810020004ULL,
		0x1000100200040208ULL,
		0x430000A044020001ULL,
		0x280009023410300ULL,
		0xE0100040002240ULL,
		0x200100401700ULL,
		0x2244100408008080ULL,
		0x8000400801980ULL,
		0x2000810040200ULL,
		0x8010100228810400ULL,
		0x2000009044210200ULL,
		0x4080008040102101ULL,
		0x40002080411D01ULL,
		0x2005524060000901ULL,
		0x502001008400422ULL,
		0x489A000810200402ULL,
		0x1004400080A13ULL,
		0x4000011008020084ULL,
		0x26002114058042ULL
	};

	const uint64_t MagicBitboards::kBishopMagics[64] = {
		0x89A1121896040240ULL,
		0x2004844802002010ULL,
		0x2068080051921000ULL,
		0x62880A0220200808ULL,
		0x4042004000000ULL,
		0x100822020200011ULL,
		0xC00444222012000AULL,
		0x28808801216001ULL,
		0x400492088408100ULL,
		0x201C401040C0084ULL,
		0x840800910A0010ULL,
		0x82080240060ULL,
		0x2000840504006000ULL,
		0x30010C4108405004ULL,
		0x1008005410080802ULL,
		0x8144042209100900ULL,
		0x208081020014400ULL,
		0x4800201208CA00ULL,
		0xF18140408012008ULL,
		0x1004002802102001ULL,
		0x841000820080811ULL,
		0x40200200A42008ULL,
		0x800054042000ULL,
		0x88010400410C9000ULL,
		0x520040470104290ULL,
		0x1004040051500081ULL,
		0x2002081833080021ULL,
		0x400C00C010142ULL,
		0x941408200C002000ULL,
		0x658810000806011ULL,
		0x188071040440A00ULL,
		0x4800404002011C00ULL,
		0x104442040404200ULL,
		0x511080202091021ULL,
		0x4022401120400ULL,
		0x80C0040400080120ULL,
		0x8040010040820802ULL,
		0x480810700020090ULL,
		0x102008E00040242ULL,
		0x809005202050100ULL,
		0x8002024220104080ULL,
		0x431008804142000ULL,
		0x19001802081400ULL,
		0x200014208040080ULL,
		0x3308082008200100ULL,
		0x41010500040C020ULL,
		0x4012020C04210308ULL,
		0x208220A202004080ULL,
		0x111040120082000ULL,
		0x6803040141280A00ULL,
		0x2101004202410000ULL,
		0x8200000041108022ULL,
		0x21082088000ULL,
		0x2410204010040ULL,
		0x40100400809000ULL,
		0x822088220820214ULL,
		0x40808090012004ULL,
		0x910224040218C9ULL,
		0x402814422015008ULL,
		0x90014004842410ULL,
		0x1000042304105ULL,
		0x10008830412A00ULL,
		0x2520081090008908ULL,
		0x40102000A0A60140ULL
	};

	const uint64_t MagicBitboards::kRookShifts[] = {
		52, 53, 53, 53, 53, 53, 53, 52,
		53, 54, 54, 54, 54, 54, 54, 53,
		53, 54, 54, 54, 54, 54, 54, 53,
		53, 54, 54, 54, 54, 54, 54, 53,
		53, 54, 54, 54, 54, 54, 54, 53,
		53, 54, 54, 54, 54, 54, 54, 53,
		53, 54, 54, 54, 54, 54, 54, 53,
		52, 53, 53, 53, 53, 53, 53, 52
	};

	const uint64_t MagicBitboards::kBishopShifts[64] = {
		58, 59, 59, 59, 59, 59, 59, 58,
		59, 59, 59, 59, 59, 59, 59, 59,
		59, 59, 57, 57, 57, 57, 59, 59,
		59, 59, 57, 55, 55, 57, 59, 59,
		59, 59, 57, 55, 55, 57, 59, 59,
		59, 59, 57, 57, 57, 57, 59, 59,
		59, 59, 59, 59, 59, 59, 59, 59,
		58, 59, 59, 59, 59, 59, 59, 58
	};

	Bitboard MagicBitboards::rook_relevant_occupancy_masks_[64];
	Bitboard MagicBitboards::bishop_relevant_occupancy_masks_[64];
	std::vector<Bitboard> MagicBitboards::rook_attack_table_[64];
	std::vector<Bitboard> MagicBitboards::bishop_attack_table_[64];

	/// <summary>
	/// Initializes this instance.
	/// </summary>
	void MagicBitboards::Init()
	{
		// Right angles, excluding source
		const auto north_and_east_a1 = 0x01010101010101FE;
		const auto south_and_west_h8 = 0x7F80808080808080;
		// Diagonal rays, excluding source
		const auto north_east_a1 = 0x8040201008040200ULL;
		const auto north_west_h1 = 0x0102040810204000ULL;
		const auto south_east_a8 = 0x0002040810204080ULL;
		const auto south_west_h8 = 0x0040201008040201ULL;
		// Inner 6x6 area. Used to mask outer squares because they don't affect bishop (and often rook) attacks
		const auto edge_mask = 0x007e7e7e7e7e7e00ULL;
		// All squares except a1, a8, h1 and h8. Used to mask corners because they don't affect rook attacks
		const auto corner_mask = 0x7effffffffffff7e;
		// Board edges excluding corners, used because outer squares do affect rook attacks from outer squares
		const auto file_a = 0x0101010101010101ULL;
		const auto file_h = 0x8080808080808080ULL;
		const auto rank_1 = 0x00000000000000FFULL;
		const auto rank_8 = 0xFF00000000000000ULL;

		for(auto square = 0; square < 64; ++square)
		{
			const auto bb = BitboardTools::SingleSquare(square);

			// Rook
			const auto north_and_east_ax = BitboardTools::ShiftUp(north_and_east_a1, SquareTools::Rank(square));
			const auto north_and_east = BitboardTools::ShiftRight(north_and_east_ax, SquareTools::File(square));

			const auto south_and_west_hx = BitboardTools::ShiftDown(south_and_west_h8, 7 - SquareTools::Rank(square));
			const auto south_and_west = BitboardTools::ShiftLeft(south_and_west_hx, 7 - SquareTools::File(square));

			auto mask = edge_mask;
			// If the square is on an edge, don't exclude that edge from relevant occupancy
			if (bb & file_a)
				mask |= file_a;
			else if (bb & file_h)
				mask |= file_h;
			if (bb & rank_1)
				mask |= rank_1;
			else if (bb & rank_8)
				mask |= rank_8;
			// But still exclude corners
			mask &= corner_mask;
			rook_relevant_occupancy_masks_[square] = (north_and_east | south_and_west) & mask;

			// Populate attack table
			const auto rook_occupancy_bits = 64 - kRookShifts[square];
			const auto rook_occupancies = 1 << rook_occupancy_bits;
			auto& rook_attacks = rook_attack_table_[square];
			rook_attacks.resize(rook_occupancies);
			for(auto i = 0; i < rook_occupancies; ++i)
			{
				const auto occupancy = SpreadBits(i, rook_relevant_occupancy_masks_[square]);
				const auto hash = (occupancy * kRookMagics[square]) >> kRookShifts[square];
				rook_attacks[hash] = PrecalcBitboards::RookAttacks(square, occupancy);
			}

			// Bishop
			const auto north_east_ax = BitboardTools::ShiftUp(north_east_a1, SquareTools::Rank(square));
			const auto north_east = BitboardTools::ShiftRight(north_east_ax, SquareTools::File(square));

			const auto north_west_hx = BitboardTools::ShiftUp(north_west_h1, SquareTools::Rank(square));
			const auto north_west = BitboardTools::ShiftLeft(north_west_hx, 7 - SquareTools::File(square));

			const auto south_east_ax = BitboardTools::ShiftDown(south_east_a8, 7 - SquareTools::Rank(square));
			const auto south_east = BitboardTools::ShiftRight(south_east_ax, SquareTools::File(square));

			const auto south_west_hx = BitboardTools::ShiftDown(south_west_h8, 7 - SquareTools::Rank(square));
			const auto south_west = BitboardTools::ShiftLeft(south_west_hx, 7 - SquareTools::File(square));

			bishop_relevant_occupancy_masks_[square] = (north_east | north_west | south_east | south_west) & edge_mask;

			// Populate attack table
			const auto bishop_occupancy_bits = 64 - kBishopShifts[square];
			const auto bishop_occupancies = 1 << bishop_occupancy_bits;
			auto& bishop_attacks = bishop_attack_table_[square];
			bishop_attacks.resize(bishop_occupancies);
			for (auto i = 0; i < rook_occupancies; ++i)
			{
				const auto occupancy = SpreadBits(i, bishop_relevant_occupancy_masks_[square]);
				const auto hash = (occupancy * kBishopMagics[square]) >> kBishopShifts[square];
				bishop_attacks[hash] = PrecalcBitboards::BishopAttacks(square, occupancy);
			}
		}
	}

	Bitboard MagicBitboards::SpreadBits(const Bitboard dense, const Bitboard sparse_mask)
	{
		auto ret = BitboardTools::kEmpty;
		auto remaining_mask = sparse_mask;
		for(auto i = 0; remaining_mask; ++i)
		{
			const auto val = (dense >> i) & 1;

			const auto square = BitboardTools::BitScanForward(remaining_mask);
			const auto bb = BitboardTools::SingleSquare(square);

			if(val)
				ret |= bb;
			remaining_mask &= ~bb;
		}
		return ret;
	}

}
